<?php

namespace Tests\Feature;

use App\Ticket;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DashboardTests extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        $ticket1 = factory(Ticket::class)->create([
            "created_at" => Carbon::parse("-1 days"),
            "status"     => Ticket::STATUS_SOLVED
        ]);
        $ticket2 = factory(Ticket::class)->create([
            "created_at" => Carbon::parse("-5 days"),
            "status"     => Ticket::STATUS_SOLVED
        ]);
        $ticket3 = factory(Ticket::class)->create([
            "created_at" => Carbon::parse("-5 days"),
            "status"     => Ticket::STATUS_NEW
        ]);
    }

    /** @test */
    public function it_returns_tickets_opened_this_year()
    {
        $response = $this->json('PUT', '/api/settings/user', [
            'name' => 'Changed User Test',
            'email' => 'changedtester@gmail.com'
        ]);

        $this->assertTrue(true);
    }
}
