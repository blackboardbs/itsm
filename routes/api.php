<?php

Route::group(['namespace' => 'Api', 'middleware' => 'apiAuth'], function () {
    Route::resource('tickets', 'TicketsController', ['except' => 'destroy']);
    Route::post('tickets/{ticket}/comments', 'CommentsController@store');
    Route::post('tickets/{ticket}/assign', 'TicketAssignController@store');
    Route::post('teams', 'TeamController@store');
    Route::get('teams', 'TeamController@index');
    Route::get('teams/{team}/tickets', 'TeamTicketsController@index');
    Route::get('teams/{team}/leads', 'TeamLeadsController@index');

    Route::resource('leads', 'LeadsController', ['only' => 'store']);

    Route::resource('ideas', 'IdeasController', ['only' => ['store', 'index']]);
});

Route::group(['namespace' => 'Api', 'middleware' => 'apiAdmin'], function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('opened','DashboardController@opened');
        Route::get('resolved/sla','DashboardController@resolvedSla');
        Route::get('resolved/avg','DashboardController@resolvedAvg');
    });
});

Route::get('help/qs.jsonp','Help\HelpQuestionsController@qs');

