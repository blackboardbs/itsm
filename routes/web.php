<?php

//Route::get('/', 'HomeController@index');

Route::redirect('/', 'login');

Auth::routes();

Route::get('auth/user/switch', 'UsersController@switchBack')->name('switch.back');

Route::group(['prefix' => 'requester'], function () {
    Route::get('tickets/view/{token}', 'Requester\RequesterTicketsController@show')->name('requester.tickets.show');
    Route::post('tickets/{token}/comments', 'Requester\RequesterCommentsController@store')->name('requester.comments.store');
    Route::get('tickets/{token}/rate', 'Requester\RequesterTicketsController@rate')->name('requester.tickets.rate');
    Route::get('tickets/create', 'Requester\RequesterTicketsController@create')->name('requester.tickets.create');
    Route::post('tickets/save', 'Requester\RequesterTicketsController@save')->name('requester.tickets.store');
});

Route::post('webhook/bitbucket', 'WebhookController@store');

Route::group(['middleware' => ['auth', 'userLocale']], function () {
    Route::get('actions/resolve/{ticket}', 'Actions\ActionsController@resolve')->name('ac.resolve');
    Route::post('actions/store', 'Actions\ActionsController@save')->name('ac.resolve.store');

    Route::get('profile', 'ProfileController@show')->name('profile.show');
    Route::put('profile', 'ProfileController@update')->name('profile.update');
    Route::post('password', 'ProfileController@password')->name('profile.password');
    Route::post('photo', 'ProfileController@photo')->name('profile.photo');

    Route::get('tickets/merge', 'Tickets\TicketsMergeController@index')->name('tickets.merge.index');
    //Route::post('tickets/merge', 'Tickets\TicketsMergeController@store')->name('tickets.merge.store');
    Route::get('tickets/search/{text}', 'Tickets\TicketsSearchController@index')->name('tickets.search');
    Route::resource('tickets', 'Tickets\TicketsController', ['except' => ['edit', 'destroy']]);
    Route::post('tickets/{ticket}/assign', 'Tickets\TicketsAssignController@store')->name('tickets.assign');
    Route::post('tickets/{ticket}/comments', 'CommentsController@store')->name('comments.store');
    Route::resource('tickets/{ticket}/tags', 'Tickets\TicketsTagsController', ['only' => ['store', 'destroy'], 'as' => 'tickets']);
    Route::post('tickets/{ticket}/reopen', 'Tickets\TicketsController@reopen')->name('tickets.reopen');

    Route::post('tickets/{ticket}/escalate', 'Tickets\TicketsEscalateController@store')->name('tickets.escalate.store');
    Route::delete('tickets/{ticket}/escalate', 'Tickets\TicketsEscalateController@destroy')->name('tickets.escalate.destroy');

    Route::post('tickets/{ticket}/issue', 'Tickets\TicketsIssueController@store')->name('tickets.issue.store');
    Route::post('tickets/{ticket}/idea', 'Tickets\TicketsIdeaController@store')->name('tickets.idea.store');

//    Route::resource('leads', 'Leads\LeadsController');
//    Route::get('leads/search/{text}', 'Leads\LeadsSearchController@index')->name('leads.search');
//    Route::post('leads/{lead}/assign', 'Leads\LeadAssignController@store')->name('leads.assign');
//    Route::post('leads/{lead}/status', 'Leads\LeadStatusController@store')->name('leads.status.store');
//    Route::resource('leads/{lead}/tags', 'Leads\LeadTagsController', ['only' => ['store', 'destroy'], 'as' => 'leads']);
//    Route::resource('leads/{lead}/tasks', 'Leads\LeadTasksController', ['only' => ['index', 'store', 'update', 'destroy'], 'as' => 'leads']);

    Route::resource('tasks', 'TasksController', ['only' => ['index', 'update', 'destroy']]);

    Route::resource('teams', 'TeamsController');
    Route::get('teams/{team}/agents', 'TeamAgentsController@index')->name('teams.agents');
    Route::get('teams/{token}/join', 'TeamMembershipController@index')->name('membership.index');
    Route::post('teams/{token}/join', 'TeamMembershipController@store')->name('membership.store');

    Route::get('dashboard', 'Reports\DashboardController@index')->name('dash.index');
    Route::get('slareport', 'Reports\SLAReportController@index')->name('sla.report');

    Route::group(['middleware' => 'can:see-admin'], function () {
        Route::resource('ideas', 'IdeasController');
        Route::get('roadmap', 'RoadmapController@index')->name('roadmap.index');
        Route::resource('ideas/{idea}/tags', 'IdeaTagsController', ['only' => ['store', 'destroy'], 'as' => 'ideas']);
        Route::post('ideas/{idea}/issue', 'IdeaIssueController@store')->name('ideas.issue.store');

        Route::resource('users', 'UsersController');
        Route::post('users/resend', 'UsersController@sendResetLinkEmail')->name('users.password.reset');
        Route::get('users/{user}/impersonate', 'UsersController@impersonate')->name('users.impersonate');
        Route::resource('settings', 'SettingsController', ['only' => ['edit', 'update']]);

        Route::resource('questions', 'Help\HelpQuestionsController', ['as' => 'qs']);
        Route::resource('help', 'Help\HelpAnswersController', ['as' => 'hp']);
        Route::resource('actions', 'MasterData\SupportedActionsController', ['as' => 'ac']);
        Route::resource('cause', 'MasterData\RootCauseController', ['as' => 'rc']);
        Route::resource('company', 'Supported\CompanyController', ['as' => 'co']);
        Route::resource('customer', 'Supported\CustomerController', ['as' => 'cu']);
        Route::resource('solution', 'Supported\SolutionController', ['as' => 'so']);
        Route::resource('sla', 'Reports\SLAController', ['only' => ['index', 'store', 'update']]);
    });

    Route::get('reports', 'ReportsController@index')->name('reports.index');

    Route::group(['prefix' => 'incidents'], function () {
        Route::get('all', 'IncidentsController@index')->name('incidents.index');
    });
});
