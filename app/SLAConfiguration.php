<?php

namespace App;

class SLAConfiguration extends BaseModel
{
    protected $table = 'sla_configurations';

    const MEASURE_MINUTES = 'minutes';
    const MEASURE_HOURS = 'hours';
    const MEASURE_DAYS = 'days';

    public static function measures()
    {
        return [
            self::MEASURE_MINUTES => 'Minutes',
            self::MEASURE_HOURS => 'Hours',
            self::MEASURE_DAYS => 'Days'
        ];
    }
}
