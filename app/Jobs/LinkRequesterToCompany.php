<?php

namespace App\Jobs;

use App\Company;
use App\Requester;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LinkRequesterToCompany implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $customers = Requester::where('company_id', null)->get();
        $customers->each(function($customer) {
            $domain = explode('@', $customer->email)[1];
            $company = Company::where('email','like', "%@{$domain}")->first();
            if ($company != null) {
                $customer->update(['company_id' => $company->id]);
                $this->log("{$customer->name} linked to company {$company->name}");
            }
        });
    }

    private function log($message) {
        \Log::info("LinkRequesterToCustomer: ".$message);
    }
}
