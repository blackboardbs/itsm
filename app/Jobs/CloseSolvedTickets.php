<?php

namespace App\Jobs;

use App\Ticket;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CloseSolvedTickets implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $thresholdDays;

    public function __construct($thresholdDays = 4)
    {
        $this->thresholdDays = $thresholdDays;
    }

    public function handle()
    {
        $tickets = Ticket::whereStatus(Ticket::STATUS_SOLVED)
                ->where('created_at', '<', Carbon::parse("-{$this->thresholdDays} days"));

        \Log::info("CloseSolvedTickets: {$tickets->count()} will now be closed");
        $tickets->update(['status' => Ticket::STATUS_CLOSED]);
        \Log::info("CloseSolvedTickets: tickets successfully closed");
    }
}
