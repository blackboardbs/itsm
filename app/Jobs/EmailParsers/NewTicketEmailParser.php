<?php

namespace App\Jobs\EmailParsers;

use App\HelpQuestions;
use App\Solution;
use App\Ticket;
use App\Attachment;

class NewTicketEmailParser
{
    public function handle($message)
    {
        $help = HelpQuestions::first();
        $solution = Solution::first();

        $details = [
            'name' => $message->fromName,
            'email' => $message->fromAddress,
            'title' => $message->subject,
            'body' => $message->body(),
            'priority' => $help->priority,
            'department' => $help->team,
            'url' => '',
            'solution_id' => $solution->id
        ];

        $ticket = Ticket::createAndNotifyAll($details, ['email']);
        Attachment::storeAttachmentsFromEmail($message, $ticket);

        return true;
    }
}
