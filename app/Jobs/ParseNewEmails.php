<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Services\Pop3\Mailbox;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\EmailParsers\NewIdeaEmailParser;
use App\Jobs\EmailParsers\NewTicketEmailParser;
use App\Jobs\EmailParsers\NewCommentEmailParser;

class ParseNewEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $messagesParsed = 0;

    protected $handlers = [
        NewIdeaEmailParser::class,
        NewCommentEmailParser::class,
        NewTicketEmailParser::class,
    ];

    public function handle(Mailbox $pop3)
    {
        $pop3->login(
            config('mail.fetch.host'),
            config('mail.fetch.port'),
            config('mail.fetch.username'),
            config('mail.fetch.password'),
            'INBOX',
            config('mail.fetch.useSSL'),
            config('mail.fetch.options')
        );

        $this->log('Success login, next -> get messages');

        try {
            $pop3->getMessages()->each(function ($message) use ($pop3) {
                $this->processMessage($message);
                $pop3->delete($message->id);
                $this->messagesParsed++;
            });

            $this->log('Messages processed');
            $pop3->expunge();
        } catch (\Exception $ex) {
            $this->log('Something wrong '.$ex->getMessage());
            $pop3->expunge();
        }
    }

    private function log($message)
    {
        \Log::info("ParseMail: " . $message);
    }

    private function processMessage($message)
    {
        \Log::info("Processing message");
        collect($this->handlers)->first(function ($handler) use ($message) {
            return (new $handler)->handle($message);
        });
    }
}
