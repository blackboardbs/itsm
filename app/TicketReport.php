<?php

namespace App;

class TicketReport extends BaseModel
{
    protected $table = 'ticket_reports';

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class,'ticket_id');
    }

    public function sla()
    {
        return $this->belongsTo(SLAConfiguration::class, 'sla_id', 'id');
    }

    public function startEvent()
    {
        return Ticket::statusNameFor($this->start_event);
    }

    public function endEvent()
    {
        return Ticket::statusNameFor($this->end_event);
    }

    public function timeTaken($measure)
    {
        switch ($measure) {
            case SLAConfiguration::MEASURE_MINUTES:
                return $this->created_at->diffInMinutes($this->before_event);
            case SLAConfiguration::MEASURE_HOURS:
                return $this->created_at->diffInHours($this->before_event);
            case SLAConfiguration::MEASURE_DAYS:
                return $this->created_at->diffInDays($this->before_event);
            default:
                return $this->created_at->diffForHumans($this->before_event);
        }
    }

    public function slaRating()
    {
        $ticket = $this->ticket->priority;

        if ($this->sla == null) {
            $finalTime = $this->timeTaken(-1);
            $sla = '<span class="label label-warning">No SLA</span>';
            $rating = '<span class="label label-warning">None</span>';
            $difference = '<span class="label label-warning">No Diff</span>';
        } else {
            $timeTaken = $this->timeTaken($this->sla->measure);
            $diff = $this->sla->unit - $timeTaken;
            $diffOutput = $this->sla->unit - $timeTaken . ' ' . $this->sla->measure;
            $difference = $diff > 0 ? '<span class="label label-success">' . $diffOutput . '</span>' : '<span class="label label-danger">' . $diffOutput . '</span>';
            $finalTime = $timeTaken . ' ' . $this->sla->measure . ' [' . $this->created_at->diffForHumans($this->before_event) . ']';
            $rating = $timeTaken <= $this->sla->unit ? '<span class="label label-success">In SLA</span>' : '<span class="label label-danger">Not Met</span>';
            $sla = $this->sla->unit . ' ' . $this->sla->measure;
        }

        return (object)[
            'timeTaken' => $finalTime,
            'sla' => $sla,
            'difference' => $difference,
            'rating' => $rating
        ];
    }
}
