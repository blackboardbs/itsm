<?php

namespace App\Http\Controllers\Tickets;

use App\Repositories\TicketsRepository;
use App\Http\Controllers\Controller;

class TicketsSearchController extends Controller
{
    public function index(TicketsRepository $repository, $text)
    {
        return view('tickets.indexTable', ['tickets' => $repository->search($text)->latest('updated_at')->paginate(50)]);
    }
}
