<?php

namespace App\Http\Controllers\Tickets;

use App\Tag;
use App\Ticket;
use App\Http\Controllers\Controller;

class TicketsTagsController extends Controller
{
    public function store(Ticket $ticket)
    {
        $ticket->attachTags([request('tag')]);

        return response()->json();
    }

    public function destroy(Ticket $ticket, $tag)
    {
        $ticket->detachTag($tag);

        return response()->json();
    }
}
