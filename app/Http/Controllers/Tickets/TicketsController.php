<?php

namespace App\Http\Controllers\Tickets;

use App\Ticket;
use App\Solution;
use App\Repositories\TicketsIndexQuery;
use App\Repositories\TicketsRepository;
use BadChoice\Thrust\Controllers\ThrustController;
use BadChoice\Thrust\Facades\Thrust;
use BadChoice\Thrust\Html\Edit;
use BadChoice\Thrust\ResourceGate;
use App\Http\Controllers\Controller;

class TicketsController extends Controller
{
    public function index()
    {
//        return (new ThrustController)->index('tickets');
        $resourceName = 'tickets';

        $resource = Thrust::make($resourceName);
        app(ResourceGate::class)->check($resource, 'index');

        if ($resource::$singleResource){
            return $this->singleResourceIndex($resourceName, $resource);
        }

        return view('tickets.index', [
            'resourceName' => $resourceName,
            'resource' => $resource,
            'searchable' => count($resource::$search) > 0,
            'description' => $resource->getDescription(),
            'sortable'  => $resource::$sortable,
            'fields'    => $this->getIndexFields($resource),
            'rows'      => $resource->rows()
        ]);

//        return view('tickets.index',[
//            'resourceName' => $resourceName,
//            'resource' => $resource,
//            'searchable' => count($resource::$search) > 0,
//            'description' => $resource->getDescription(),
//        ]);

    }

    public function getIndexFields($resource)
    {
        return $resource->fieldsFlattened()->where('showInIndex', true);
    }

    /*public function index(TicketsRepository $repository)
    {
        $ticketsQuery = TicketsIndexQuery::get($repository);
        $ticketsQuery = $ticketsQuery->select('tickets.*')->latest('updated_at');

        return view('tickets.index', ['tickets' => $ticketsQuery->paginate(25, ['tickets.user_id'])]);
    }*/

    public function show(Ticket $ticket)
    {
        $this->authorize('view', $ticket);

        return view('tickets.show', ['ticket' => $ticket]);
    }

    public function create()
    {
        $solutions = Solution::pluck('name','id');
        return view('tickets.create', ['solutions'=>$solutions]);
    }

    public function store()
    {
        $validated = $this->validate(request(), [
            'name'      => 'required|min:3',
            'email'     => 'required|email',
            'title'     => 'required|min:3',
            'body'      => 'required|min:3',
            'team_id'   => 'required|integer|exists:teams,id',
            'solution_id' => 'required|integer|exists:supported_solutions,id',
            'url' => 'required|url',
            'status'    => 'required|integer'
        ]);

        $ticket = Ticket::createAndNotify($validated, request('tags'));

        return redirect()->route('tickets.show', $ticket);
    }

    public function reopen(Ticket $ticket)
    {
        $ticket->updateStatus(Ticket::STATUS_OPEN);

        return back();
    }

    public function update(Ticket $ticket)
    {
        $this->validate(request(), [
            'requester' => 'required|array',
            'priority'  => 'required|integer',
            //'title'      => 'required|min:3',
        ]);
        $ticket->updateWith(request('requester'), request('priority'));

        return back();
    }
}
