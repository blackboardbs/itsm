<?php

namespace App\Http\Controllers\Reports;

use App\SLAConfiguration;
use App\Http\Controllers\Controller;

class SLAController extends Controller
{
    public function index() {
        $configurations = SLAConfiguration::all();
        return view('reports.sla.index', compact('configurations'));
    }

    public function store() {
        $valid = $this->validate(request(), [
            'unit' => 'required',
            'measure' => 'required'
        ]);

        foreach ($valid['unit'] as $key => $value) {
            $config = SLAConfiguration::find($key);
            $config->unit = $value;
            $config->measure = $valid['measure'][$key];
            $config->save();
        }

        return redirect()->route('sla.index')->with(['success'=>'SLA updated successfully']);
    }
}
