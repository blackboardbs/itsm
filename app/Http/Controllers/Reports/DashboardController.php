<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;

class DashboardController extends Controller
{
    public function index() {
        $companies = Company::all();
        return view('reports.dashboard.index',compact('companies'));
    }
}
