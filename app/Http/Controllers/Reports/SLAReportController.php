<?php

namespace App\Http\Controllers\Reports;

use App\SLAConfiguration;
use App\TicketReport;
use App\Ticket;
use App\Http\Controllers\Controller;

class SLAReportController extends Controller
{
    public function index() {
        $user = auth()->user();

        $reports = TicketReport::where('user_id',$user->id)->get();

        $ids = $reports->pluck('ticket_id');
        $tickets = Ticket::where('user_id', $user->id)->whereNotIn('id', $ids)->get();

        return view('reports.sla.report', ['tickets' => $tickets, 'reports' => $reports ]);
    }
}
