<?php

namespace App\Http\Controllers\Api;

use App\Ticket;
use App\Settings;
use App\Requester;
use Illuminate\Http\Response;
use App\Notifications\TicketCreated;

class TicketsController extends ApiController
{
    public function index()
    {
        $requester = Requester::whereName(request('requester'))->orWhere('email', '=', request('requester'))->firstOrFail();
        if (request('status') == 'solved') {
            $tickets = $requester->solvedTickets;
        } elseif (request('status') == 'closed') {
            $tickets = $requester->closedTickets;
        } else {
            $tickets = $requester->openTickets;
        }

        return $this->respond($tickets);
    }

    public function show(Ticket $ticket)
    {
        return $this->respond($ticket->load('requester', 'comments'));
    }

    public function store()
    {
        $validated = $this->validate(request(), [
            'name'      => 'required|min:3',
            'email'     => 'required|email',
            'title'     => 'required|min:3',
            'body'      => 'required|min:3',
            'department'   => 'required|integer|exists:teams,id',
            'priority'    => 'required|integer'
        ]);

        $validated['solution_id'] = 1;
        $validated['url'] = 'n/a';

        $ticket = Ticket::createAndNotifyAll($validated, request('tags'));

        return $this->respond(['id' => $ticket->id], Response::HTTP_CREATED);
    }

    public function update(Ticket $ticket)
    {
        $ticket->updateStatus(request('status'));

        return $this->respond(['id' => $ticket->id], Response::HTTP_OK);
    }

    private function notifyDefault($ticket)
    {
        $setting = Settings::first();
        if ($setting && $setting->slack_webhook_url) {
            $setting->notify(new TicketCreated($ticket));
        }
    }
}
