<?php

namespace App\Http\Controllers\Api;

use App\HelpQuestions;
use Illuminate\Http\Request;

class HelpQuestionsController extends ApiController
{
    public function qs(Request $request) {

        if($request->get('token') === 'api-token') {
            $questions = HelpQuestions::all();

            return response() ->json($questions)->setCallback($request->input('callback'));
        } else {
            return response() ->json(['success'=> false])->setCallback($request->input('callback'));
        }
    }
}
