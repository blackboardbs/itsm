<?php

namespace App\Http\Controllers\Api;

use App\SLAConfiguration;
use App\TicketReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ticket;
use Illuminate\Http\Response;

class DashboardController extends ApiController
{
    public function opened()
    {
        $request = request(['year']);
        $year = isset($request['year']) ? $request['year'] : date('Y');

        $open_tickets = Ticket::select(\DB::raw('MONTH(created_at) as month'), \DB::raw("(COUNT(created_at)) as total"))
            ->whereYear('created_at', $year)
            ->orderBy('month')
            ->groupBy(\DB::raw("month"))
            ->get();

        $datasets[0] = [
            'label' => 'Tickets opened',
            'data' => [],
            'backgroundColor' => [],
        ];

        $labels = [];

        foreach ($open_tickets as $open_ticket) {
            $dateObj = \DateTime::createFromFormat('!m', $open_ticket->month);
            $label = $dateObj->format('M'); // Jan

            if (!in_array($label, $labels) && !empty($labels)) {
                $last_element = end($labels);
                $month_num = (int)date('m', strtotime($last_element));
                $diffMonths = $open_ticket->month - $month_num;

                if ($diffMonths > 1) { //each month missed
                    for ($i = $month_num + 1; $i < $open_ticket->month; $i++) {
                        $datasets[0]['data'][] = 0;
                        $datasets[0]['backgroundColor'][] = '#CED4DA';

                        $dateObj = \DateTime::createFromFormat('!m', $i);
                        $labels[] = $dateObj->format('M'); // Jan
                    }
                }
            }

            $datasets[0]['data'][] = $open_ticket->total;
            $datasets[0]['backgroundColor'][] = '#007BFF';

            $labels[] = $label;
        }

        $closed_tickets = Ticket::select(\DB::raw('MONTH(updated_at) as month'), \DB::raw("(COUNT(updated_at)) as total"))
            ->whereYear('updated_at', $year)
            ->where('status', Ticket::STATUS_SOLVED)
            ->orderBy('month')
            ->groupBy(\DB::raw("month"))
            ->get();

        $datasets[1] = [
            'label' => 'Resolved tickets',
            'data' => [],
            'backgroundColor' => [],
        ];

        $labelsSolved = [];

//        $closedCount = count($closed_tickets);
//        $labelCount = count($labels);

        foreach ($closed_tickets as $closed_ticket) {
            $dateObj = \DateTime::createFromFormat('!m', $closed_ticket->month);
            $label = $dateObj->format('M'); // Jan

            if (!in_array($label, $labels)) {
                $last_element = end($labels);
                $month_num = (int)date('m', strtotime($last_element));
                $diffMonths = $closed_ticket->month - $month_num;

                if ($diffMonths > 1) { //each month missed
                    for ($i = $month_num + 1; $i < $closed_ticket->month; $i++) {
                        $datasets[1]['data'][] = 0;
                        $datasets[1]['backgroundColor'][] = '#CED4DA';

                        $dateObj = \DateTime::createFromFormat('!m', $i);
                        $labels[] = $dateObj->format('M'); // Jan
                    }
                }

                $labels[] = $label;
            } else {
                $arrIndex = array_search($label, $labels);

                $lastElementSolved = end($labelsSolved);
                if ($lastElementSolved === false) {
                    $labelsSolved[] = $label;
                    $lastElementSolved = end($labelsSolved);
                } else {
                    $labelsSolved[] = $label;
                }
                $monthNumSolved = (int)date('m', strtotime($lastElementSolved));
                $diffMonthsSolved = $closed_ticket->month - $monthNumSolved;

                if ($diffMonthsSolved < 1) {

                    for ($i = $monthNumSolved + 1; $i < $closed_ticket->month; $i++) {
                        $datasets[1]['data'][] = 0;
                        $datasets[1]['backgroundColor'][] = '#CED4DA';
                    }
                }
            }

            $datasets[1]['data'][] = $closed_ticket->total;
            $datasets[1]['backgroundColor'][] = '#CED4DA';
        }

        return $this->respond(['labels' => $labels, 'datasets' => $datasets], Response::HTTP_OK);
    }

    public function resolvedSla()
    {
        $request = request(['year']);
        $year = isset($request['year']) ? $request['year'] : date('Y');

        $ticketReport = TicketReport::whereYear('created_at', $year)->get();
        $inSlaCounter = 0;
        $outSlaCounter = 0;

        foreach ($ticketReport as $report) {
            $timeTaken = $report->timeTaken($report->sla->measure);
            $diff = $report->sla->unit - $timeTaken;
            if ($diff > 0) {
                $inSlaCounter += 1;
            } else {
                $outSlaCounter += 1;
            }
        }

        $labels = ['Resolved within SLA', 'Resolved outside SLA'];

        $datasets[0] = [
            'data' => [$inSlaCounter, $outSlaCounter],
            'backgroundColor' => ['#007BFF', '#ffaa00'],
        ];

        return $this->respond(['labels' => $labels, 'datasets' => $datasets], Response::HTTP_OK);
    }

    public function resolvedAvg()
    {
        $request = request(['year']);
        $year = isset($request['year']) ? $request['year'] : date('Y');

        $tickets = Ticket::select(\DB::raw('COUNT(priority) as total'), 'priority')
            ->where('status', Ticket::STATUS_SOLVED)
            ->whereYear('updated_at', $year)
            ->groupBy('priority')
            ->get();

        $labels = [];
        $datasets[0] = [
            'label' => 'Tickets resolved per priority',
            'data' => [],
            'backgroundColor' => [],
        ];

        foreach ($tickets as $ticket) {
            $labels[] = ucfirst(Ticket::priorityNameFor($ticket->priority));
            $datasets[0]['data'][] = $ticket->total;;
            $datasets[0]['backgroundColor'][] = '#007BFF';
        }

        return $this->respond(['labels' => $labels, 'datasets' => $datasets], Response::HTTP_OK);
    }
}
