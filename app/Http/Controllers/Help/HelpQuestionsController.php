<?php

namespace App\Http\Controllers\Help;

use App\HelpQuestions;
use App\Repositories\HelpQuestionsRepository;
use App\Team;
use App\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HelpQuestionsController extends Controller
{
    public function index(HelpQuestionsRepository $repository)
    {
        return view('questions.index', ['questions' => $repository->all()->paginate(25)]);
    }

    public function create()
    {
        $teams = Team::pluck('name','id');
        $priority = Ticket::priority();
        $routes = HelpQuestions::availableRoute();
        return view('questions.create', ['priority' => $priority, 'teams' => $teams, 'routes'=>$routes]);
    }

    public function edit(HelpQuestions $question)
    {
        $teams = Team::pluck('name','id');
        $priority = Ticket::priority();
        $routes = HelpQuestions::availableRoute();
        return view('questions.edit', ['question' => $question, 'priority' => $priority, 'teams' => $teams, 'routes'=>$routes]);
    }

    public function update(HelpQuestions $question)
    {
        $newQuestion = $this->validate(request(), [
            'question'  => 'required|min:3',
            'route' => 'required|min:3',
            'priority' => 'required|integer',
            'team' => 'required|integer'
        ]);

        $question->update($newQuestion);

        return redirect()->route('qs.questions.index');
    }

    public function store()
    {
        $question = $this->validate(request(), [
            'question'  => 'required|min:3',
            'route' => 'required|min:3',
            'priority' => 'required|integer',
            'team' => 'required|integer'
        ]);

        HelpQuestions::create($question);

        return redirect()->route('qs.questions.index');
    }

    public function qs(Request $request) {
        if($request->get('token') === 'api-token') {
            $questions = HelpQuestions::all();

            return response() ->json($questions)->setCallback($request->input('callback'));
        } else {
            return response() ->json(['success'=> false])->setCallback($request->input('callback'));
        }
    }
}
