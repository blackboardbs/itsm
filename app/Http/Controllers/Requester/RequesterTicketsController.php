<?php

namespace App\Http\Controllers\Requester;

use App\Company;
use App\HelpQuestions;
use App\SLAConfiguration;
use App\ThrustHelpers\Filters\PriorityFilter;
use App\Ticket;
use App\Team;
use App\Solution;
use Carbon\Carbon;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Repositories\TicketsRepository;

class RequesterTicketsController extends Controller
{
    public function show($public_token)
    {
        $ticket = Ticket::findWithPublicToken($public_token);
        $lastReply = Carbon::now()->diffInHours($ticket->updated_at);

        return view('requester.tickets.show', [
            'ticket' => $ticket,
            'shouldAutoClose' => $lastReply > 72,
            'supportEmail' => env('MAIL_FETCH_USERNAME', 'support@blackboardbs.com')
        ]);
    }

    public function create()
    {
        $teams = Team::pluck('name', 'id');
        $priority = (new PriorityFilter)->options();
        unset($priority['Blocker']);

        $solutions = Solution::pluck('name', 'id');
        $configs = HelpQuestions::pluck('question', 'id');

        return view('requester.tickets.create', [
            'configs' => $configs,
            'solutions' => $solutions,
            'priority' => array_flip($priority),
            'teams' => $teams,
            'company' => request('company')
        ]);
    }

    public function save()
    {
        $ticketData = $this->validate(request(), [
            'name' => 'required|min:3',
            'email' => 'required|email|min:3',
            'title' => 'required|min:3',
            'body' => 'required|min:10',
            'route' => 'required|integer|exists:help_questions,id',
            'solution_id' => 'required|integer|exists:supported_solutions,id',
            'url' => 'required|url'
        ]);

        $company = Company::find(request('company'));
        if($company != null) {
            $ticketData['company_id'] = $company->id;
        }

        $ticket = Ticket::createAndNotifyPublic($ticketData);

        return redirect()->route('requester.tickets.show', ['token' => $ticket["public_token"]]);
    }

    public function rate($public_token)
    {
        $ticket = Ticket::findWithPublicToken($public_token);
        $rated = $ticket->rate(request('rating'));
        if (!$rated) {
            app()->abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'Could not rate this ticket');
        }

        return view('requester.tickets.rated', ['ticket' => $ticket]);
    }
}
