<?php

namespace App\Http\Controllers\Requester;

use App\Ticket;
use App\Http\Controllers\Controller;

class RequesterCommentsController extends Controller
{
    public function store($public_token)
    {
        $validatedData = $this->validate(request(), [
            'body' => 'required|min:3'
        ]);

        $ticket = Ticket::findWithPublicToken($public_token);
        $ticket->addComment(null, $validatedData['body'], $this->getNewStatus());

        return back();
    }

    private function getNewStatus()
    {
        if (request('solved')) {
            return Ticket::STATUS_SOLVED;
        }
        if (request('reopen')) {
            return Ticket::STATUS_OPEN;
        }

        return null;
    }
}
