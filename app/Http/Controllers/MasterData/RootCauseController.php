<?php

namespace App\Http\Controllers\MasterData;

use Illuminate\Http\Request;
use App\RootCause;
use App\Http\Controllers\Controller;

class RootCauseController extends Controller
{
    public function index()
    {
        $rootcauses = RootCause::paginate(25);
        return view('rootcause.index', compact('rootcauses'));
    }

    public function create()
    {
        return view('rootcause.create');
    }

    public function edit(RootCause $cause)
    {
        return view('rootcause.edit', ['rootcause' => $cause]);
    }

    public function update(RootCause $cause)
    {
        $newRootCause = $this->validate(request(), [
            'name' => 'required|min:3',
            'description' => 'required|min:3'
        ]);

        $cause->update($newRootCause);

        return redirect()->route('rc.cause.index');
    }

    public function store()
    {
        $rootcause = $this->validate(request(), [
            'name' => 'required|min:3',
            'description' => 'required|min:3'
        ]);

        RootCause::create($rootcause);

        return redirect()->route('rc.cause.index');
    }

    public function destroy(RootCause $cause)
    {
        $message = '<strong>'.$cause->name.'</strong>'.' was successfully deleted';
        $cause->delete();
        return redirect()->route('rc.cause.index')->with('success', $message);
    }
}
