<?php

namespace App\Http\Controllers\MasterData;

use App\SupportedAction;
use App\Http\Controllers\Controller;

class SupportedActionsController extends Controller
{
    public function index()
    {
        $actions = SupportedAction::paginate(25);
        return view('actions.index', ['actions' => $actions]);
    }

    public function create()
    {
        return view('actions.create');
    }

    public function edit(SupportedAction $action)
    {
        return view('actions.edit', ['action' => $action]);
    }

    public function show(SupportedAction $action)
    {
        return view('actions.show', ['action' => $action]);
    }

    public function update(SupportedAction $action)
    {
        $newAction = $this->validate(request(), [
            'name' => 'required|min:3',
            'action_steps' => 'required|min:3',
            'client_script' => 'required|min:3'
        ]);

        $action->update($newAction);

        return redirect()->route('ac.actions.index');
    }

    public function store()
    {
        $action = $this->validate(request(), [
            'name' => 'required|min:3',
            'action_steps' => 'required|min:3',
            'client_script' => 'required|min:3'
        ]);

        SupportedAction::create($action);

        return redirect()->route('ac.actions.index');
    }

    public function destroy(SupportedAction $action)
    {
        $message = '<strong>'.$action->name.'</strong>'.' was successfully deleted';
        $action->delete();
        return redirect()->route('ac.actions.index')->with('warning', $message);
    }
}
