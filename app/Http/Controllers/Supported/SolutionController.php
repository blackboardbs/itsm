<?php

namespace App\Http\Controllers\Supported;

use Illuminate\Http\Request;
use App\Solution;
use App\Http\Controllers\Controller;

class SolutionController extends Controller
{
    public function index() {
        $solutions = Solution::paginate(25);
        return view('supported.solution.index', compact('solutions'));
    }

    public function create()
    {
        return view('supported.solution.create');
    }

    public function edit(Solution $solution)
    {
        return view('supported.solution.edit', ['solution' => $solution]);
    }

    public function update(Solution $solution)
    {
        $newAction = $this->validate(request(), [
            'name' => 'required|min:3',
            'version' => 'required|min:3',
            'product_owner' => 'required|min:3'
        ]);

        $solution->update($newAction);

        return redirect()->route('so.solution.index');
    }

    public function store()
    {
        $action = $this->validate(request(), [
            'name' => 'required|min:3',
            'version' => 'required|min:3',
            'product_owner' => 'required|min:3'
        ]);

        $id = \Auth::user()->id;
        $action['user_id'] = $id;

        Solution::create($action);

        return redirect()->route('so.solution.index');
    }

    public function destroy(Solution $solution)
    {
        $message = '<strong>'.$solution->name.'</strong>'.' was successfully deleted';
        $solution->delete();
        return redirect()->route('so.solution.index')->with('warning', $message);
    }
}
