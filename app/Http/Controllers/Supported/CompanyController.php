<?php

namespace App\Http\Controllers\Supported;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function index() {
        $companies = Company::paginate(25);
        return view('supported.company.index', compact('companies'));
    }

    public function create()
    {
        return view('supported.company.create');
    }

    public function edit(Company $company)
    {
        return view('supported.company.edit', ['company' => $company]);
    }

    public function update(Company $company)
    {
        $newAction = $this->validate(request(), [
            'name' => 'required|min:3',
            'address' => 'required|min:3',
            'phone' => 'required|min:3',
            'contact_number' => 'required|min:10',
            'email' => 'required|email',
            'support_email' => 'required|email',
            'web' => 'required|min:3'
        ]);

        $company->update($newAction);

        return redirect()->route('co.company.index');
    }

    public function store()
    {
        $action = $this->validate(request(), [
            'name' => 'required|min:3',
            'address' => 'required|min:3',
            'phone' => 'required|min:3',
            'contact_number' => 'required|min:10',
            'email' => 'required|email',
            'support_email' => 'required|email',
            'web' => 'required|min:3'
        ]);

        Company::create($action);

        return redirect()->route('co.company.index');
    }

    public function destroy(Company $company)
    {
        $message = '<strong>'.$company->name.'</strong>'.' was successfully deleted';
        $company->delete();
        return redirect()->route('co.company.index')->with('warning', $message);
    }

}
