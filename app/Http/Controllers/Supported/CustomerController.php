<?php

namespace App\Http\Controllers\Supported;

use App\Requester;
use App\Http\Controllers\Controller;
use App\Company;
use App\Ticket;

class CustomerController extends Controller
{
    public function index() {
        $customers = Requester::paginate(25);
        return view('supported.customer.index', compact('customers'));
    }

    public function create()
    {
        $companies = Company::pluck('name','id');
        return view('supported.customer.create', ['companies' => $companies]);
    }

    public function edit(Requester $customer)
    {
        $companies = Company::pluck('name','id');
        return view('supported.customer.edit', ['customer' => $customer, 'companies'=>$companies]);
    }

    public function update(Requester $customer)
    {
        $newAction = $this->validate(request(), [
            'name' => 'required|min:3',
            'address' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|min:5',
            'company_id' => 'required|integer'
        ]);

        $customer->update($newAction);

        return redirect()->route('cu.customer.index');
    }

    public function store()
    {
        $action = $this->validate(request(), [
            'name' => 'required|min:3',
            'address' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|min:5',
            'company_id' => 'required|integer'
        ]);

        Requester::create($action);

        return redirect()->route('cu.customer.index');
    }

    public function destroy(Requester $customer)
    {
        $message = '<strong>'.$customer->name.'</strong>'.' was successfully deleted';
        $tickets = Ticket::where('requester_id', $customer->id)->get()->count();
        if($tickets == 0) {
            $customer->delete();
        } else {
            $message = '<strong>'.$customer->name.'</strong>'.' currently has tickets and may not be deleted';
        }

        return redirect()->route('cu.customer.index')->with($tickets == 0 ? 'warning' : 'error', $message);
    }
}
