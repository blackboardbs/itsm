<?php

namespace App\Http\Controllers\Supported;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SystemController extends Controller
{
    public function index() {
        $systems = SupportedSystem::paginate(25);
        return view('supported.system.index', compact('systems'));
    }

    public function create()
    {
        return view('supported.system.create');
    }

    public function edit(SupportedSystem $system)
    {
        return view('supported.system.edit', ['system' => $system]);
    }

    public function update(SupportedSystem $system)
    {
        $newAction = $this->validate(request(), [
            'name' => 'required|min:3',
            'action_steps' => 'required|min:3',
            'client_script' => 'required|min:3'
        ]);

        $system->update($newAction);

        return redirect()->route('sys.system.index');
    }

    public function store()
    {
        $action = $this->validate(request(), [
            'name' => 'required|min:3',
            'action_steps' => 'required|min:3',
            'client_script' => 'required|min:3'
        ]);

        SupportedSystem::create($action);

        return redirect()->route('sys.system.index');
    }

    public function destroy(SupportedSystem $system)
    {
        $message = '<strong>'.$system->name.'</strong>'.' was successfully deleted';
        $system->delete();
        return redirect()->route('co.system.index')->with('warning', $message);
    }
}
