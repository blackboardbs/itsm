<?php

namespace App\Http\Controllers\Actions;

use App\Http\Controllers\Controller;
use App\Ticket;
use App\SupportedAction;
use App\RootCause;

class ActionsController extends Controller
{
    public function save()
    {
        $validate = $this->validate(request(), [
            'sendfeedback' => 'required',
            'rootcause' => 'required',
            'action' => 'required|exists:supported_actions,id',
            'id' => 'required|exists:tickets,id'
        ]);

        $ticket = Ticket::find((int)$validate['id']);

        $ticket->updateStatus(Ticket::STATUS_SOLVED);
        return redirect()->route('tickets.index')->with('success','Ticket resolved');
    }

    public function resolve(Ticket $ticket)
    {
        $this->authorize('view', $ticket);

        $actions = SupportedAction::all();
        $rootcause = RootCause::pluck('name','id');

        $actions_json = $actions->pluck('client_script','id');

        return view('actions.resolve', ['ticket' => $ticket, 'actions_json'=>$actions_json, 'actions'=>$actions->pluck('name','id'), 'rootcause' => $rootcause ]);
    }
}
