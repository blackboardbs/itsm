<?php

namespace App\Http\Controllers;

use App\Membership;
use App\User;
use App\Team;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    use SendsPasswordResetEmails;

    public function index()
    {
        $users = User::with('teams')->paginate(25);

        return view('users.index', ['users' => $users]);
    }

    public function create()
    {
        $teams = Team::pluck('name','id');
        $lead = [
          '0' => 'Not team leader',
          '1' => 'Team leader'
        ];

        return view('users.create', ['teams' => $teams, 'lead' => $lead]);
    }

    public function store()
    {
        $details = $this->validate(request(),[
            'name'=>'required|min:3',
            'email'=>'required|email|unique:users,email',
            'team' => 'required|exists:teams,id',
            'lead' => 'required|boolean'
        ]);

        $pass = str_random(16);
        $details['pass'] = $pass;

        $user = User::create([
            'name'     => $details['name'],
            'email'    => $details['email'],
            'password' => bcrypt($details['pass'])
        ]);

        Membership::create([
            'user_id' => $user->id,
            'team_id' => $details['team'],
            'admin' => $details['lead']
        ]);

        $user->newAccountNotify();

        return redirect()->route('users.index');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return back()->with('warning','User '.$user->name.' removed from the system');
    }

    public function edit(User $user)
    {

        $teams = Team::all();
        $lead = [
            '0' => 'Not team leader',
            '1' => 'Team leader'
        ];

        $user_teams = $user->teams->pluck('pivot');

        foreach($teams as $i => $team) {
            foreach ($user_teams as $user_team) {
                if($user_team->team_id == $team->id) {
                    $teams[$i]['member'] = true;
                    $teams[$i]['lead'] = $user_team->admin;
                    break;
                }
            }
        }

        return view('users.edit', ['user' => $user, 'lead'=>$lead, 'teams' => $teams]);
    }

    public function update(User $user)
    {
        $newUser = $this->validate(request(), [
            'name'  => 'required|min:3',
            'email' => 'unique:users,email,'.$user->id,
            'admin' => 'boolean',
            'assistant' => 'boolean'
        ]);

        $user->name = $newUser['name'];
        $user->email = $newUser['email'];
        $user->admin = request('admin', 0);
        $user->assistant = request('assistant', 0);

        $user->update();

        // Update teams and roles
        $teams = [];
        $rq_teams = request('team', []);
        $leads = request('team_lead', []);

        foreach ($rq_teams as $i => $team) {
            $teams[$i]['user_id'] = $user->id;
            $teams[$i]['team_id'] = $team;
            $teams[$i]['admin'] = in_array($team, $leads);
        }

        Membership::where('user_id', $user->id)->delete();
        \DB::table("memberships")->insert($teams);

        return redirect()->route('users.index')->with('success', 'User successfully updated');
    }

    public function impersonate(Request $request, User $user)
    {
        $request->session()->put('impersonator', \Auth::user()->id);

        auth()->loginUsingId($user->id);

        return redirect()->route('tickets.index');
    }

    public function switchBack(Request $request)
    {
        $user_id = $request->session()->get('impersonator', false);

        if($user_id) {
            auth()->loginUsingId($user_id);
            $request->session()->forget('impersonator');
        }

        return redirect()->route('tickets.index');
    }
}
