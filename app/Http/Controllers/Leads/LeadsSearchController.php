<?php

namespace App\Http\Controllers\Leads;

use App\Repositories\LeadsRepository;
use App\Http\Controllers\Controller;

class LeadsSearchController extends Controller
{
    public function index(LeadsRepository $repository, $text)
    {
        return view('leads.indexTable', ['leads' => $repository->search($text)->latest('updated_at')->paginate(50)]);
    }
}
