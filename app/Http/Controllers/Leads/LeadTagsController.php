<?php

namespace App\Http\Controllers\Leads;

use App\Lead;
use App\Http\Controllers\Controller;

class LeadTagsController extends Controller
{
    public function store(Lead $lead)
    {
        $lead->attachTags([request('tag')]);

        return response()->json();
    }

    public function destroy(Lead $lead, $tag)
    {
        $lead->detachTag($tag);

        return response()->json();
    }
}
