<?php

namespace App;

class HelpQuestions extends BaseModel
{
    const SUPPORT_SYSTEM = 'support_system';
    const HELP_PORTAL = 'help_portal';

    public static function availableRoute()
    {
        return [
            static::SUPPORT_SYSTEM => 'Support System',
            static::HELP_PORTAL => 'Help Portal'
        ];
    }

    public function hteam()
    {
        return $this->belongsTo(Team::class, 'team', 'id');
    }
}
