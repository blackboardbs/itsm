<?php

namespace App\Services\Pop3;

use App\User;
use App\Ticket;

class IncomingMailCommentParser
{
    public $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function checkIfItIsACommentAndGetTheTicket()
    {
        if (! $this->isAComment()) {
            return null;
        }

        $ticket = Ticket::find($this->getTicketId());
        if(!$this->isEmailValid($ticket)) return null;
        return $ticket;
    }

    public function isEmailValid($ticket) {
        $result = false;
        $ticket_id = $ticket != null ? $ticket->id : null;

        if($ticket_id != null) {
            $fromEmail = $this->message->fromAddress;
            if ($fromEmail == $ticket->requester->email) {
                $result = true;
            } else {
                $result = User::where('email', $fromEmail)->first() != null;
            }
        }

        \Log::info("ParseEmail: email sender validation {$result} : {$this->message->fromAddress} : ticket_id : {$ticket_id}");

        return $result;
    }

    public function isAComment()
    {
        return str_contains($this->message->body(), config('mail.fetch.replyAboveLine'));
    }

    public function getCommentBody()
    {
        return strstr($this->message->body(), config('mail.fetch.replyAboveLine'), true);
    }

    public function getTicketId()
    {
        preg_match('~ticket-id:(\d+)(\.)~', $this->message->body(), $results);

        return  (count($results) > 1) ? $results[1] : null;
    }

    public function getUser($ticket)
    {
        $fromEmail = $this->message->fromAddress;
        if ($fromEmail == $ticket->requester->email) {
            return null;
        }

        return User::where('email', $fromEmail)->first();
    }
}
