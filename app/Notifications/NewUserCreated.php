<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewUserCreated extends Notification
{
    use Queueable;
    public $user;

    /**
     * Create a new notification instance.
     * @param $user User
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("New user created for {$this->user->name}")
            ->replyTo(config('mail.fetch.address'))
            ->view('emails.newUser', [
                    'title'   => 'New account created',
                    'user'  => $this->user,
                    'url'     => route('password.request'),
                ]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
