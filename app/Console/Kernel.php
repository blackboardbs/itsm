<?php

namespace App\Console;

use App\Customer;
use App\Requester;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CloseSolvedTickets::class,
        Commands\ParseNewEmails::class,
        Commands\SendDailyTasksEmail::class,
        Commands\LinkRequesterToCompany::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('bbSupport:closeSolvedTickets')->dailyAt('23:57');
        $schedule->command('bbSupport:parseNewEmails')->everyMinute()->withoutOverlapping();
        $schedule->command('bbSupport:sendDailyTasksEmail')->dailyAt('6:30');
        $schedule->command('bbSupport:linkRequesterToCompany')->sundays()->when(function() {
            return Requester::where('company_id', null)->first() != null;
        });
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
