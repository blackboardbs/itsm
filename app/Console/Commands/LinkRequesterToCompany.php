<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LinkRequesterToCompany extends Command
{
    protected $signature   = 'bbSupport:linkRequesterToCompany';
    protected $description = 'Links the current requesters to company';

    public function handle()
    {
        \App\Jobs\LinkRequesterToCompany::dispatch();
        $this->info('Requesters linked');
    }
}
