<?php

namespace App\Repositories;

use App\HelpQuestions;
use Carbon\Carbon;

class HelpQuestionsRepository
{
    public function all()
    {
        return HelpQuestions::where('status',true);
    }

    public function search($text)
    {
        $leadsQuery = HelpQuestions::query();

        return $leadsQuery->where(function ($query) use ($text) {
            $query->where('question', 'like', "%{$text}%");
        });
        /*->distinct()*/
    }
}
