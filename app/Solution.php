<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solution extends BaseModel
{
    protected $table = 'supported_solutions';
}
