<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends BaseModel
{
    protected $table = 'supported_customers';

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

}
