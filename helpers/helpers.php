<?php

use Carbon\Carbon;

function createSelectArray($array, $withNull = false)
{
    if (! $array) {
        return [];
    }
    $values = $array->pluck('name', 'id')->toArray();
    if ($withNull) {
        return ['' => '--'] + $values;
    }

    return $values;
}

function nameOrDash($object)
{
    return ($object && $object->name) ? $object->name : '--';
}

function icon($icon)
{
    return FA::icon($icon);
}

function ticketStatus($status, $name) {
    switch($status) {
        case App\Ticket::PRIORITY_NORMAL:
            return "<label class='label label-info'>$name</label>";
        case App\Ticket::STATUS_NEW:
            return "<label class='label label-primary'>$name</label>";
        case App\Ticket::STATUS_CLOSED:
            return "<label class='label label-danger'>$name</label>";
        case App\Ticket::STATUS_SOLVED:
            return "<label class='label label-success'>$name</label>";
        default:
            return "<label class='label label-primary'>$name</label>";
    }
}

function profilePhoto($full = false, $photo = null)
{
    $photo_path = $photo != null && $photo != 'user.png' ? $photo : Auth::user()->photo;
    $photo_url = $photo == 'user.png' ? '/'.$photo : Storage::url($photo_path);
    if($photo == null && Auth::user()->photo == 'user.png')
    {
        $photo_url = '/user.png';
    }

    $styles = $full ? 'style="width: 100px;height: 100px"' : 'style="width:35px;height:35px; float: left"';
    return '<img src="'.$photo_url.'" 
                 alt="User profile" 
                 class="img img-responsive img-circle" 
                 '.$styles.'
            />';
}

//TODO: Remove or re purpose
function gravatar($email, $size = 30)
{
    $gravatarURL  = gravatarUrl($email, $size);

    return '<img id = '.$email.''.$size.' class="gravatar" src="'.$gravatarURL.'" width="'.$size.'">';
}

//TODO: Remove this
function gravatarUrl($email, $size)
{
    $email = md5(strtolower(trim($email)));
    //$gravatarURL = "https://www.gravatar.com/avatar/" . $email."?s=".$size."&d=mm";
    $defaultImage = urlencode('https://raw.githubusercontent.com/BadChoice/handesk/master/public/images/default-avatar.png');

    return 'https://www.gravatar.com/avatar/'.$email.'?s='.$size."&default={$defaultImage}";
}

function toTime($minutes)
{
    $minutes_per_day = (Carbon::HOURS_PER_DAY * Carbon::MINUTES_PER_HOUR);
    $days            = floor($minutes / ($minutes_per_day));
    $hours           = floor(($minutes - $days * ($minutes_per_day)) / Carbon::MINUTES_PER_HOUR);
    $mins            = (int) ($minutes - ($days * ($minutes_per_day)) - ($hours * 60));

    return "{$days} Days {$hours} Hours {$mins} Mins";
}

function toPercentage($value, $inverse = false)
{
    return  ($inverse ? 1 - $value : $value) * 100;
}
