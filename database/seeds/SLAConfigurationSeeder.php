<?php

use Illuminate\Database\Seeder;

use App\SLAConfiguration;
use App\Ticket;

class SLAConfigurationSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //from new to open
        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_HIGH,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_NEW,
            'end_event' => Ticket::STATUS_OPEN
        ]);

        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_NORMAL,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_NEW,
            'end_event' => Ticket::STATUS_OPEN
        ]);

        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_LOW,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_NEW,
            'end_event' => Ticket::STATUS_OPEN
        ]);


        //from open to pending
        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_HIGH,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_OPEN,
            'end_event' => Ticket::STATUS_PENDING
        ]);

        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_NORMAL,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_OPEN,
            'end_event' => Ticket::STATUS_PENDING
        ]);

        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_LOW,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_OPEN,
            'end_event' => Ticket::STATUS_PENDING
        ]);

        //from pending to solved
        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_HIGH,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_PENDING,
            'end_event' => Ticket::STATUS_SOLVED
        ]);

        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_NORMAL,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_PENDING,
            'end_event' => Ticket::STATUS_SOLVED
        ]);

        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_LOW,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_PENDING,
            'end_event' => Ticket::STATUS_SOLVED
        ]);

        //from solved to closed
        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_HIGH,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_SOLVED,
            'end_event' => Ticket::STATUS_CLOSED
        ]);

        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_NORMAL,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_SOLVED,
            'end_event' => Ticket::STATUS_CLOSED
        ]);

        SLAConfiguration::create([
            'priority' => Ticket::PRIORITY_LOW,
            'unit' => 1,
            'measure' => SLAConfiguration::MEASURE_HOURS,
            'start_event' => Ticket::STATUS_SOLVED,
            'end_event' => Ticket::STATUS_CLOSED
        ]);
    }
}
