<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SupportedSystems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supported_systems', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('solution_id');
            $table->unsignedInteger('customer_id');
            $table->dateTime('install_date');
            $table->string('current_version');
            $table->string('support_contact');
            $table->string('support_email');
            $table->string('support_number');
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supported_systems');
    }
}
