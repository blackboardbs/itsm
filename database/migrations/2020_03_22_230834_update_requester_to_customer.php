<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRequesterToCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requesters', function($table) {
            $table->unsignedInteger('company_id')->after('id')->nullable();
            $table->boolean('status')->after('email')->default(true);
            $table->string('address')->after('email')->default('');
            $table->string('phone')->after('email')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requesters', function($table) {
            $table->dropColumn('company_id');
            $table->dropColumn('address');
            $table->dropColumn('status');
            $table->dropColumn('phone');
        });
    }
}
