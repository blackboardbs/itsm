let mix = require('laravel-mix');

mix.js([
    'resources/assets/js/app.js',
    // 'vendor/badchoice/thrust/src/resources/js/thrust.min.js',
    // 'resources/assets/js/utils.js',
    // 'resources/assets/js/libs/jquery.tagsinput.min.js',  //http://xoxco.com/projects/code/tagsinput/
    'resources/assets/js/libs/mention.js/bootstrap-typeahead.js',  //https://github.com/ivirabyan/jquery-mentions
    'resources/assets/js/libs/mention.js/mention.js',  //https://github.com/ivirabyan/jquery-mentions
    'node_modules/gentelella/build/js/custom.js'
], 'public/js/app.js')
    .js('resources/assets/js/libs/chart.js','public/js/chart.js')
    .sass('resources/assets/sass/app.scss', '../resources/assets/css/style.css')
    .sass('resources/assets/sass/public.scss', 'public/css/app.css')
    .styles([
        'resources/assets/css/libs/jquery.tagsinput.min.css',
        'resources/assets/css/style.css'
    ],'public/css/all.css');
