
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(document).on("click","form.delete_form > button",function(event){
    event.preventDefault();
    var form = $(this).parents('form:first');
    bootbox.confirm("<h2>Are you sure you want to delete? This action cannot be undone.</h2>", function(result) {
        if(result === true) { form.submit(); }
    });
});

setupFormLoadingImage();
function setupFormLoadingImage(){
    $('form').submit(function(event){
        $('.busy').show('fast');
        return true;
    });
}
