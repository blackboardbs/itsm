@extends('layouts.app')
@section('content')
    <div class="page-title">
        <h3>Root Cause</h3>
    </div>

    <div class="row">
        <a class="btn btn-primary" href="{{ route("rc.cause.create") }}">@icon(plus) Root cause</a>
    </div>

    @paginator($rootcauses)
    <table class="table table-hover table-striped table-condensed">
        <thead>
        <tr>
            <th> Name</th>
            <th> Description</th>
            <th> Updated </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse($rootcauses as $rootcause)
            <tr>
                <td> {{ $rootcause->name}}</td>
                <td> {{ str_limit($rootcause->description, 50, '...')}} </td>
                <td> {{ $rootcause->updated_at->diffForHumans() }}</td>
                <td>
                    @can('update', $rootcause)
                        {{--<a class="btn btn-xs btn-default" href="{{route('rc.cause.edit', $rootcause)}}">@icon(pencil)</a>--}}
                    @endcan
                        <a class="btn btn-xs btn-default" href="{{route('rc.cause.edit', $rootcause)}}">@icon(pencil)</a>
                        @include('components.deleteButton', ['url' => 'rc.cause.destroy', 'model' => $rootcause, 'btn_class' => 'btn-xs'])
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6" class="empty-cell">No root causes defined</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    @paginator($rootcauses)
@endsection
