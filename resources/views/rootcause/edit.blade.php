@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ route('rc.cause.index') }}"> Root cause </a>
        </div>
    </div>

    <h3>#{{ $rootcause->id }} {{ $rootcause->name }}</h3>

    <div class="row">
        <div class="col-md-6">
            {{ Form::open(["url" => route('rc.cause.update', $rootcause), 'method' => 'PUT']) }}

            <div class="form-group">
                <label for="name">Description:</label>
                <input class="form-control" type="text" name="name" value="{{$rootcause->name}}" placeholder="Description" required/>
            </div>

            <div class="form-group">
                <label for="description">Action Steps:</label>
                <textarea class="form-control" name="description" placeholder="Description" rows="3" required>
                    {{$rootcause->description}}
                </textarea>
            </div>

            <a href="{{route('rc.cause.index')}}" class="btn btn-default"> Cancel</a>
            <button class="btn btn-primary" type="submit"> Update</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
