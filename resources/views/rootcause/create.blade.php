@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ route('rc.cause.index') }}"> Root Cause </a>
        </div>
    </div>

    <h3>Create root cause</h3>

    <div class="row">
        <div class="col-md-6">
            {{ Form::open(["url" => route("rc.cause.store")]) }}

            <div class="form-group">
                <label for="name">Description:</label>
                <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Name"
                       required/>
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" placeholder="Description" rows="3" required>
                    {{ old('description') }}
                </textarea>
            </div>

            {{--@include('components.mergeFields')--}}

            <button class="btn btn-primary" type="submit"> Save</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
