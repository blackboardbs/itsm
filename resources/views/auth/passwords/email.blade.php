@extends('auth.layout')

@section('title')
    Reset Password
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label>E-Mail Address:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="email" type="email" class="form-control" name="email" placeholder="Email Address"  value="{{ old('email') }}" required autofocus />
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-block">
                        Send Password Reset Link
                    </button>
                </div>

                <div class="text-center">
                    <a class="btn btn-link" href="{{ route('login') }}"> Remembered your password? Log In </a>
                </div>

            </form>
        </div>
    </div>
@endsection
