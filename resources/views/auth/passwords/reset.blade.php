@extends('auth.layout')

@section('title')
    Reset Password
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group">
                    <label for="email" class="control-label">E-Mail Address:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="email" type="email" class="form-control" name="email" placeholder="Email Address"  value="{{ $email or old('email') }}" required autofocus />
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="control-label">Password:</label>
                    <div class="input-group">
                        <input id="password" type="password" class="form-control" name="password" required>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="control-label">Confirm Password</label>
                    <div class="input-group">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-block">
                        Reset Password
                    </button>
                </div>

                <div class="text-center">
                    <a class="btn btn-link" href="{{ route('login') }}">Log In </a>
                </div>

            </form>
        </div>
    </div>
@endsection
