@extends('auth.layout')

@section('title')
    Sign In
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="email">Email Address:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="email" type="email" class="form-control" name="email" placeholder="Email Address"  value="{{ old('email') }}" required autofocus />
                    </div>
                </div>

                <div class="form-group">
                    <label for="password">Password: </label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="password" type="password" class="form-control" placeholder="Password" name="password" required />
                    </div>
                </div>

                <div class="form-group">
                    <input type="checkbox" class="" name="remember" {{ old('remember') ? 'checked' : '' }}>
                    Remember Me
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-block">
                        <i class="fa fa-btn fa-sign-in"></i> Sign In
                    </button>
                </div>

                <div class="text-center">
                    <a class="btn btn-link" href="{{ route('password.request') }}"> Forgot Your Password? </a>
                </div>

            </form>
        </div>
    </div>
@endsection
