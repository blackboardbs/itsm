@extends('auth.layout')

@section('title')
    Create your account
@endsection

@section('content')
    <div class="row">
        <p class="center"> {{ __('team.invitedDesc') }}</p>
        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Name:</label>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required
                       autofocus>
                @if ($errors->has('name'))
                    <span class="error">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                @endif
            </div>

            <div class="form-group">
                <label for="email">E-Mail Address:</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="error">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input id="password" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <span class="error">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                @endif
            </div>

            <div class="form-group">
                <label for="password-confirm">Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>

            <div class="form-group">
                {{--<input id="team_token" type="text" class="form-control disabled" name="team_token"--}}
                       {{--value="{{request('team_token')}}" disabled required placeholder="INVITATION CODE">--}}
                <input type="hidden" value="{{request('team_token')}}" name="team_token" />
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Register</button>
            </div>
        </form>
    </div>
@endsection
