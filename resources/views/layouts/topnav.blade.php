<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                @if(session('impersonator', false))
                    <li>
                        <a href="{{ route('switch.back') }}"
                           title="You are currently impersonating a user, click here to switch back to the original admin account">
                            @icon(key) Switch back
                        </a>
                    </li>
                @endif
                <li>
                    <a href="#" class="user-profile dropdown-toggle" aria-expanded="false" data-toggle="dropdown">
                        {!! profilePhoto() !!}
                        {{ auth()->user()->name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li>
                            <a href="{{route('profile.show')}}">@icon(user) Profile</a>
                        </li>
                        <li>
                            {{ Form::open(["url" => route('logout')]) }}
                            <button class="btn btn-block btn-default text-left" style="border: 0;border-radius: 0">
                                @icon(sign-out) Log out
                            </button>
                            {{ Form::close() }}
                        </li>
                    </ul>
                </li>

            </ul>
        </nav>
    </div>
</div>