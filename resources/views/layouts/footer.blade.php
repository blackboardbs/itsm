<footer>
    <div class="pull-right">
        <p>Powered by Blackboard Support Engine Copyright &copy; {{date('Y')}}</p>
    </div>
    <div class="clearfix"></div>
</footer>