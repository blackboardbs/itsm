<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title">
            <a href="/" class="site_logo">
                <img src="{{ url("logo.jpg") }}" class="img img-responsive">
            </a>
        </div>
        <div class="clearfix"></div>

        @if (Auth::check())
            <div class="profile clearfix">
                <div>
                    {!! profilePhoto() !!}
                    <h2 class="profile_name">{{ auth()->user()->name }}</h2>
                </div>
            </div>
        @endif

        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a href="{{route('dash.index')}}">@icon(tachometer) Dashboard</a>
                    </li>
                    {{--<li>--}}
                    {{--<a href="#">@icon(calendar) Calender</a>--}}
                    {{--</li>--}}
                    @include('components.sidebarItem', ["url" => route('tickets.index')."?recent=true", "title" => 'Recent', 'icon' => 'history'])
                    @include('layouts.sidebar.tickets')

                    {{--@if (config('handesk.leads'))--}}
                    {{--@include('layouts.sidebar.leads')--}}
                    {{--@endif--}}

                    {{--@if (config('handesk.roadmap'))--}}
                    {{--@include('layouts.sidebar.roadmap')--}}
                    {{--@endif--}}

                    <li>
                        <a> @icon(bar-chart) {{ trans_choice('report.report', 2) }}<span
                                    class="fa fa-chevron-down"></span> </a>
                        <ul class="nav child_menu">
                            @include('components.sidebarItem', ["url" => route('reports.index'), "title" => trans_choice('report.report', 2) ])
                            @include('components.sidebarItem', ["url" => route('sla.report'), "title" => 'SLA' ])
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="menu_section">
                <h3>{{ trans_choice('admin.admin', 2) }}</h3>
                <ul class="nav side-menu">
                    <li>
                        <a> @icon(user-circle-o) User <span class="fa fa-chevron-down"></span> </a>
                        <ul class="nav child_menu">
                            @include('components.sidebarItem', ["url" => route('teams.index'), "title" => 'Teams'])
                            @if (Auth::check())
                                @if(auth()->user()->admin)
                                    @include('components.sidebarItem', [ "url" => route('users.index'), "title" => trans_choice('ticket.user',2)])
                                @endif
                            @endif
                            @include('components.sidebarItem', ["url" => route('profile.show'), "title" => 'Profile'])
                        </ul>
                    </li>

                    @if (Auth::check())
                        @if(auth()->user()->admin)
                            <li>
                                <a> @icon(ticket) Supported <span class="fa fa-chevron-down"></span> </a>
                                <ul class="nav child_menu">
                                    @include('components.sidebarItem', ["url" => route('co.company.index'), "title" => 'Company' ])
                                    @include('components.sidebarItem', ["url" => route('cu.customer.index'), "title" => 'Customers' ])
                                    @include('components.sidebarItem', ["url" => route('so.solution.index'), "title" => 'Solutions' ])
                                </ul>
                            </li>
                            <li>
                                <a> @icon(database) Master data <span class="fa fa-chevron-down"></span> </a>
                                <ul class="nav child_menu">
                                    @include('components.sidebarItem', ["url" => route('ac.actions.index'), "title" => 'Actions' ])
                                    @include('components.sidebarItem', ["url" => route('rc.cause.index'), "title" => 'Root cause' ])
                                </ul>
                            </li>
                            <li>
                                <a href="{{route('sla.index')}}">@icon(flag) SLA configuration</a>
                            </li>
                            <li>
                                <a href="{{route('qs.questions.index')}}">@icon(info-circle) Configurations</a>
                            </li>
                            @include('components.sidebarItem', [
                                "url" => route('settings.edit', 1),
                                "title" => trans_choice('setting.setting',  2),
                                "icon" => 'cog'
                            ])

                        @endif
                    @endif
                </ul>
            </div>
        </div>

    </div>

</div>

