@if(auth()->user()->admin)
    <li>
        <a> @icon(road) {{ __('idea.roadmap') }}<span class="fa fa-chevron-down"></span> </a>
        <ul class="nav child_menu">
        @include('components.sidebarItem', ["url" => route('ideas.index').'?pending=true',      "title" => trans_choice('idea.pendingIdea',        2) ])
        @include('components.sidebarItem', ["url" => route('ideas.index').'?ongoing=true',      "title" => trans_choice('idea.idea',        2) ])
        @include('components.sidebarItem', ["url" => route('roadmap.index'),      "title" => trans_choice('idea.roadmap', 1) ])
        </ul>
    </li>
@endif
