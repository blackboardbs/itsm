@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ url()->previous() }}">{{ trans_choice('ticket.ticket', 2) }}</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-9">
            <h3 class="page-title">Create Ticket</h3>
                    {{ Form::open(["url" => route("tickets.store")]) }}
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" value="{{  old('name') }}" required/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" value="{{  old('email') }}" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title">Subject</label>
                        <input type="text" class="form-control" name="title" value="{{ old('title') }}" required />
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="solution_id">Application:</label>
                            {{ Form::select('solution_id', $solutions, null , ['class'=>'form-control']) }}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tags">URL:</label>
                            <input type="text" name="url" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="body">Body</label>
                        <textarea class="form-control" name="body" rows="5" required>{{ old('body') }}</textarea>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            @include('components.assignTeamField')
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="status">{{ __('ticket.status') }}</label>
                            {{ Form::select("status", [
                                App\Ticket::STATUS_NEW      => __("ticket.new"),
                                App\Ticket::STATUS_OPEN     => __("ticket.open"),
                                App\Ticket::STATUS_PENDING  => __("ticket.pending"),
                            ],null,['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tags">{{ trans_choice('ticket.tag', 2)}}:</label>
                        <input type="text" name="tags" class="form-control" id="tags"/>
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label for="priority">Priority</label>--}}
                        {{--{!! Form::select('priority', $priority, null , ['class'=>'form-control']) !!}--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="attachment">@icon(paperclip) Attachment</label>
                        <input type="file" name="attachment" id="inputAttachment" class="form-control">
                    </div>

                    <button class="btn btn-primary" type="submit"> @busy Open Ticket</button>
                    {{ Form::close() }}
        </div>
    </div>
@endsection


@section('scripts')
    @include('components.js.taggableInput', ["el" => "tags", "endpoint" => "tickets", "object" => null])
@endsection