@extends('layouts.app')
@section('content')
    <h3 class="title">
        @if (isset($parent_id) )
            @php $parent = $resource->parent($parent_id) @endphp
            <a href="{{route('thrust.index', [app(\BadChoice\Thrust\ResourceManager::class)->resourceNameFromModel($parent) ]) }}">{{ $parent->name }} </a>
            /
        @endif
        {{ trans_choice(config('thrust.translationsPrefix') . str_singular($resourceName), 2) }}
        ({{ $resource->count() }})
    </h3>

    @include('thrust::components.mainActions')
{{--    {{ $description ?? "" }}--}}

{{--    @include('thrust::components.filters')--}}
{{--    @include('thrust::components.actions')--}}

    @if (count($rows) > 0)
        <table class="table table-hover table-striped table-condensed">
            <thead class="sticky">
            {{--<th class="hide-mobile">--}}
            {{--<input type="checkbox" onclick="toggleSelectAll(this)">--}}
            {{--</th>--}}
            @if ($sortable)
                <th class="hide-mobile"></th>
            @endif
            @foreach($fields as $field)
                <th class="{{$field->rowClass}}">
                    <div class='sortableHeader'>{{ $field->getTitle() }}
                        @if ($field->sortable && !request('search'))
                            <div class='sortArrows'>
                                <a href='{{ BadChoice\Thrust\ResourceFilters\Sort::link($field->field, 'desc')}}'
                                   class='sortUp'>▲</a>
                                <a href='{{ BadChoice\Thrust\ResourceFilters\Sort::link($field->field, 'asc')}}'
                                   class='sortDown'>▼</a>
                            </div>
                        @endif
                    </div>
                </th>
            @endforeach
            <th class="action text-right" colspan="2">
                @include('thrust::components.tableDensity')
            </th>
            </thead>

            <tbody class="@if($sortable) sortable @endif">
            @foreach ($rows as $row)
                <tr id="sort_{{$row->id}}">
                    {{--<td class="action"><input class='actionCheckbox' type="checkbox" name="selected[{{$row->id}}]" meta:id="{{$row->id}}"/></td>--}}
                    @if ($sortable)
                        <td class="sort action hide-mobile"></td>
                    @endif
                    @foreach($fields as $field)
                        <td class="{{$field->rowClass}}">
                            @if (! $field->shouldHide($row))
                                {!! $field->displayInIndex($row) !!}
                            @endif
                        </td>
                    @endforeach

                    <td class="action">
                        @if($row->status == 4)
                            <a class="btn btn-xs btn-default disabled">Action Taken</a>
                        @else
                            <a class="btn btn-xs btn-default" href="{{ route('ac.resolve', $row->id) }}"
                               title="Take Action">@icon(paper-plane) Action</a>
                        @endif{{--@if ($resource->canEdit($row))--}}
                        {{--<a class='btn btn-xs btn-default showPopup edit' href="{{route('thrust.edit', [$resource->name(), $row->id]) }}">@icon(pencil)</a>--}}
                        {{--@endif--}}

                        {{--@if ($resource->canDelete($row))--}}
                        {{--<a class="btn btn-xs btn-danger delete-resource" data-delete="confirm resource" href="{{route('thrust.delete', [$resource->name(), $row->id])}}">@icon(trash)</a>--}}
                        {{--@endif--}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <span>@icon(table) No Data Found</span>
            </div>
        </div>
    @endif
    @paginator($rows)
@endsection