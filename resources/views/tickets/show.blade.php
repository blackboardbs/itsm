@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ route('tickets.index') }}">{{ trans_choice('ticket.ticket', 2) }}</a>
        </div>

        <h3>
            #{{ $ticket->id }}. {{ $ticket->title }}
            <span class="pull-right">
            @if($ticket->status == 4)
                <a class="btn btn-sm btn-default disabled">Action Taken</a>
            @else
                <a class="btn btn-sm btn-default" href="{{ route('ac.resolve', $ticket->id) }}"
                   title="Take Action">@icon(paper-plane) Action</a>
            @endif
            </span>
        </h3>
        <hr />

        {{--<div class="row">--}}
        {{--<div class="col-md-6">--}}
        {{--@include('components.ticket.actions')--}}
        {{--</div>--}}
        {{--<div class="col-md-6">--}}
        {{--@include('components.ticket.merged')--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-title"><h3>Ticket information</h3></div>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <div class="truncate">#{{ $ticket->id }} - {{ $ticket->title }}</div>
                            {!! ticketStatus($ticket->status, $ticket->statusName()) !!}
                        </div>
                        <div class="list-group-item">
                            <label>Requested by</label>
                            <div class="value">
                                {{  $ticket->requester->name }} &lt;{{$ticket->requester->email}}&gt;
                            </div>
                        </div>
                        <div class="list-group-item">
                            <label>Team</label>
                            <div class="value"> {{$ticket->team->name}}</div>
                        </div>
                        <div class="list-group-item">
                            <label>Submitted</label>
                            <div class="value"> {{$ticket->created_at }}</div>
                        </div>
                        <div class="list-group-item">
                            <label>Last updated</label>
                            <div class="value"> {{$ticket->updated_at->diffForHumans() }}</div>
                        </div>
                        <div class="list-group-item">
                            <label>Priority</label>
                            <div class="value"> {{ __("ticket." . $ticket->priorityName() ) }}</div>
                        </div>
                        <div class="list-group-item">
                            @include('components.ticket.rating')
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                @if( $ticket->canBeEdited() )
                    @include('components.assignActions', ["endpoint" => "tickets", "object" => $ticket])
                    <div class="panel panel-default">
                        <div class="panel-body">
                            {{ Form::open(["url" => route("comments.store", $ticket) , "files" => true, "id" => "comment-form"]) }}

                            <div class="form-group">
                                <label for="body">Message</label>
                                <textarea class="form-control" id="comment-text-area" name="body" rows="5"></textarea>
                            </div>
                            @include('components.uploadAttachment', ["attachable" => $ticket, "type" => "tickets"])
                            {{ Form::hidden('new_status', $ticket->status, ["id" => "new_status"]) }}
                            @if($ticket->isEscalated() )
                                <button class="btn btn-primary"> @busy @icon(comment) {{ __('ticket.note') }} </button>
                            @else
                                <div class="form-group">
                                    {{ __('ticket.note') }}: {{ Form::checkbox('private') }}
                                </div>

                                <div class="btn-group">
                                    <button class="btn btn-primary">
                                        @busy @icon(comment) {{ __('ticket.commentAs') }} {{ $ticket->statusName() }}
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a class="pointer" onClick="setStatusAndSubmit( {{ App\Ticket::STATUS_OPEN    }} )">
                                                {{ __('ticket.commentAs') }} <b>{{ __("ticket.open") }}</b>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="pointer" onClick="setStatusAndSubmit( {{ App\Ticket::STATUS_PENDING }} )">
                                                {{ __('ticket.commentAs') }} <b>{{ __("ticket.pending") }}</b>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="pointer" onClick="setStatusAndSubmit( {{ App\Ticket::STATUS_SOLVED  }} )">
                                                {{ __('ticket.commentAs') }} <b>{{ __("ticket.solved") }} </b>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @endif
                            {{ Form::close() }}
                        </div>
                    </div>
                @endif

                @include('components.ticketComments', ["comments" => $ticket->commentsAndNotesAndEvents()->sortBy('created_at')->reverse() ])
            </div>

            {{--<div class="col-md-8">--}}
            {{--<div id="ticket-edit" class="hidden">--}}
            {{--{{ Form::open(["url" => route("tickets.update", $ticket) ,"method" => "PUT"]) }}--}}
            {{--<select name="priority" class="form-control">--}}
            {{--<option value="{{\App\Ticket::PRIORITY_LOW}}"--}}
            {{--@if($ticket->priority == App\Ticket::PRIORITY_LOW) selected @endif >{{ __("ticket.low") }}</option>--}}
            {{--<option value="{{\App\Ticket::PRIORITY_NORMAL}}"--}}
            {{--@if($ticket->priority == App\Ticket::PRIORITY_NORMAL) selected @endif >{{ __("ticket.normal") }}</option>--}}
            {{--<option value="{{\App\Ticket::PRIORITY_HIGH}}"--}}
            {{--@if($ticket->priority == App\Ticket::PRIORITY_HIGH) selected @endif >{{ __("ticket.high") }}</option>--}}
            {{--<option value="{{\App\Ticket::PRIORITY_BLOCKER}}"--}}
            {{--@if($ticket->priority == App\Ticket::PRIORITY_BLOCKER) selected @endif >{{ __("ticket.blocker") }}</option>--}}
            {{--</select>--}}
            {{--<input name="requester[name]" class="form-control" value="{{  $ticket->requester->name }}">--}}
            {{--<input name="requester[email]" class="form-control" value="{{$ticket->requester->email}}">--}}
            {{--<button class="btn btn-default btn-xs"> {{ __("ticket.update")}} </button>--}}
            {{--{{ Form::close() }}--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
@endsection

@section('scripts')
    {{--@include('components.js.taggableInput', ["el" => "tags", "endpoint" => "tickets", "object" => $ticket])--}}

    <script>
        function setStatusAndSubmit(new_status) {
            $("#new_status").val(new_status);
            $("#comment-form").submit();
        }

        $("#comment-text-area").mention({
            delimiter: '@',
            emptyQuery: true,
            typeaheadOpts: {
                items: 10 // Max number of items you want to show
            },
            users: {!! json_encode(App\Services\Mentions::arrayFor(auth()->user())) !!}
        });

    </script>
@endsection