@extends('layouts.app')
@section('content')
    <div class="description">
        <h3> {{ __('user.profile') }}</h3>
    </div>

    <div class="row">
        <h4 class="col-md-6 col-sm-6">
            @if($user->assistant)
                <span class="gold">@icon(star)</span>
            @endif
            {{ $user->name }}
        </h4>
        <h4 class="col-md-6 col-sm-6">{{$user->email}}</h4>
    </div>

    <hr/>

    <div class="row">
        <div class="col-md-6 col-xs-12">
            {{ Form::open( ["url" => route('profile.update'), 'method' => 'PUT'] ) }}
            <div class="form-group">
                <label for="name">{{ __('user.name')}}</label>
                {{ Form::text('name', $user->name, ["class" => "form-control", 'required'=>'required', 'placeholder'=>'Name']) }}
            </div>

            <div class="form-group">
                <label for="email">{{ __('user.email')}}</label>
                {{ Form::email('email', $user->email, ["class" => "form-control", 'required'=>'required', 'placeholder'=>'Email']) }}
            </div>

            <div class="form-group">
                <label for="locale">{{ __('user.language')}}</label>
                {{ Form::select('locale',App\Language::available(), $user->locale, ["class" => "form-control", 'required'=>'required']) }}
            </div>

            <hr/>

            <div class="form-group">
                <h2>{{ trans_choice('user.notification', 2) }}</h2>
            </div>

            <hr/>
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label for="new_ticket_notification"> {{ __('user.newTicketNotification')}} </label>
                        {{ Form::checkbox('new_ticket_notification',true, $user->settings->new_ticket_notification) }}
                    </div>

                    <div class="form-group">
                        <label for="ticket_assigned_notification"> {{ __('user.ticketAssignedNotification')}} </label>
                        {{ Form::checkbox('ticket_assigned_notification',true, $user->settings->ticket_assigned_notification) }}
                    </div>

                    <div class="form-group">
                        <label for="ticket_updated_notification"> {{ __('user.ticketUpdatedNotification')}} </label>
                        {{ Form::checkbox('ticket_updated_notification',true, $user->settings->ticket_updated_notification) }}
                    </div>

                    <div class="form-group">
                        <label for="lead_assigned_notification">{{ __('user.newLeadNotification')       }}</label>
                        {{ Form::checkbox('lead_assigned_notification',true, $user->settings->lead_assigned_notification) }}
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label for="lead_assigned_notification"> {{ __('user.leadAssignedNotification')}} </label>
                        {{ Form::checkbox('lead_assigned_notification',true, $user->settings->leadAssignedNotification) }}
                    </div>

                    <div class="form-group">
                        <label for="daily_tasks_notification"> {{ __('user.dailyTasksNotification')}} </label>
                        <input type="checkbox" name="daily_tasks_notification"
                               @if( $user->settings->daily_tasks_notification ) checked @endif>
                    </div>

                    <div class="form-group">
                        <label for="mention_notification"> {{ __('user.mentionNotification')}} </label>
                        <input type="checkbox" name="mention_notification"
                               @if( $user->settings->mention_notification ) checked @endif>
                    </div>

                    <div class="form-group">
                        <label for="ticket_rated_notification"> {{ __('user.ticketRated')}} </label>
                        <input type="checkbox" name="ticket_rated_notification"
                               @if( $user->settings->ticket_rated_notification ) checked @endif>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="form-group">
                <label for="tickets_signature">{{ __('user.ticketsSignature')}}</label>
                <textarea name="tickets_signature" class="form-control"
                          required="required"> {{ $user->settings->tickets_signature }} </textarea>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <button class="btn btn-block btn-primary">@busy {{ __('ticket.update') }}</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            {!! profilePhoto(true) !!}
                        </div>
                        <div class="col-md-9">
                            {{ Form::open(['url' => route('profile.photo'), 'files' => true]) }}
                            <div class="form-group">
                                {!! Form::label('photo','Upload profile photo') !!}
                                {!! Form::file('photo') !!}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-sm btn-primary">@icon(picture-o) Upload</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::open( ["url" => route('profile.password')] ) }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">@icon(key) {{ __('user.changePassword') }}</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="old">{{ __('user.oldPassword') }}</label>
                        {{ Form::password('old', ["class" => "form-control", "placeholder"=>'Old password','required' => 'required']) }}
                    </div>
                    <div class="form-group">
                        <label for="password">{{ __('user.newPassword') }}</label>
                        {{ Form::password('password', ["class" => "form-control",'placeholder'=>'New password', 'required' => 'required']) }}
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">{{ __('user.confirmPassword') }}</label>
                        {{ Form::password('password_confirmation', ["class" => "form-control",'placeholder'=>'Confirm password', 'required' => 'required']) }}
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-primary"> @busy {{ __('user.changePassword') }}</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>

@endsection
