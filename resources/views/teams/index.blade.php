@extends('layouts.app')
@section('content')
    <div class="description">
        <h3 class="page-title"> {{ trans_choice('team.team', 2) }} ( {{ $teams->count() }} )</h3>
    </div>

    @if(auth()->user()->admin)
        <div class="col-md-4">
            <a class="btn btn-primary" href="{{ route("teams.create") }}">@icon(plus) {{ __('team.new') }} team</a>
        </div>
    @endif

    @paginator($teams)
    <table class="table table-hover table-striped table-condensed" id="teams">
        <thead>
        <tr>
            <th class="small"></th>
            <th> {{ trans_choice('team.team',1) }}</th>
            <th> {{ trans_choice('team.email',1) }}</th>
            <th> {{ trans_choice('team.member',2) }}</th>
            <th> {{ trans_choice('team.slack',1) }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($teams as $team)
            <tr>
                <td class="small"> @gravatar($team->email)</td>
                <td> {{ $team->name }}</td>
                <td><a href="mailto:{{ $team->email }}">{{ $team->email }}</a></td>
                <td><a href="{{route('teams.agents',$team)}}">{{ $team->members->count() }}</a></td>
                <td> @if($team->slack_webhook_url) @icon(check) @else @icon(times) @endif </td>
                <td>
                    @can('administrate', $team)
                        <a class="btn btn-xs btn-default" href="{{route('membership.store',$team->token)}}">@icon(send) {{ __("team.invitationLink") }}</a>
                        <div class="hidden"
                             id="register-link-{{$team->id}}"> {{ route('membership.store',$team->token)}} </div>
                    @endcan
                    <a class="btn btn-xs btn-default" href="{{route('tickets.index')}}?team={{$team->id}}"> @icon(inbox) Tickets </a>

                @can('administrate', $team)
                    <a class="btn btn-xs btn-default" href="{{route('teams.edit',$team)}}"> @icon(pencil) Edit</a>
                @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @paginator($teams)
@endsection
