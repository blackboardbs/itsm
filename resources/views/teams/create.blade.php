@extends('layouts.app')
@section('content')
    <div class="description">
        <a href="{{ url()->previous() }}">Teams</a>
    </div>
    <div class="comment description actions mb4">
        <h3>New Team</h3>
    </div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::open(["url" => route('teams.store')]) }}
            <div class="form-group">
                <label for="name">{{ __("team.name") }}</label>
                <input class="form-control" name="name">
            </div>
            <div class="form-group">
                <label for="email">{{ __("team.email") }}</label>
                <input class="form-control" name="email">
            </div>
            <div class="form-group">
                <label for="slack_webhook_url">{{ __("team.slack_webhook_url") }}:</label>
                <input class="form-control" name="slack_webhook_url">
            </div>
            <div class="form-group">
                <button class="btn btn-primary"> @busy Create new team</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
