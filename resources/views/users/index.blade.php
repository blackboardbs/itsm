@extends('layouts.app')
@section('content')
    <div class="description">
        <h3>{{ trans_choice('ticket.user',2) }} ( {{ $users->count() }} )</h3>
    </div>

    <a class="btn btn-primary" href="{{route('users.create')}}">@icon(plus) {{ __('team.new') }} user</a>

    @paginator($users)
    <table class="table table-hover table-striped table-condensed" id="usertable">
        <thead>
        <tr>
            <th class="small"></th>
            <th> {{ trans_choice('team.name',1) }}      </th>
            <th> {{ trans_choice('team.email',2) }}     </th>
            <th> {{ trans_choice('team.team',2) }}      </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td> @include("components.avatar",["user" => $user]) </td>
                <td> {{ $user->name }} </td>
                <td> {{ $user->email }}</td>
                <td> {{ implode(", ", $user->teams->pluck('name')->toArray() ) }}</td>
                <td style="max-width: 170px">
                    <div class="row">
                        @if(!session('impersonator', false))
                            <div class="col-xs-4">
                                <a class="btn btn-xs btn-default" href="{{ route('users.impersonate', $user) }}">
                                    @icon(key)
                                    Impersonate </a>
                            </div>
                        @endif
                        <div class="col-xs-4">
                            <a class="btn btn-xs btn-default" href="{{ route('users.edit', $user) }}"> @icon(edit)
                                Edit </a>
                        </div>
                        <div class="col-xs-4">
                            {{ Form::open(['method' => 'delete', "url" => route('users.destroy', $user)]) }}
                            <button type="submit" class="btn btn-xs btn-danger"
                                    href="{{ route('users.destroy', $user) }}"
                                    class="delete-resource"> @icon(trash) Remove
                            </button>
                            {{ Form::close() }}
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @paginator($users)
@endsection
