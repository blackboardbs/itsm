@extends('layouts.app')
@section('content')
    <div class="description">
        <h3>
            Update user ({{$user->name}})
        </h3>

        {{ Form::open(['method' => 'post', "url" => route('users.password.reset', $user)]) }}
        <input type="hidden" name="email" value="{{ $user->email }}"/>
        <button type="submit" class="btn btn-link">@icon(key) Send password reset link</button>
        {{ Form::close() }}

    </div>

    <hr/>

    <div class="row">
        {{ Form::open(['method' => 'PUT', "url" => route('users.update', $user)]) }}

        <div class="col-md-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-xs-4">
                            {!! profilePhoto(true, $user->photo) !!}
                        </div>
                        <div class="col-md-9 col-xs-8">
                            <br/>
                            {!! Form::label('photo','Profile photo') !!}
                            <p>{{ $user->name }} <br/> {{ $user->email }}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="name">{{ __("team.name") }}</label>
                <input type="text" class="form-control" name="name" value="{{ $user->name }}" placeholder="User name"
                       required="required">
            </div>
            <div class="form-group">
                <label for="email">{{ __("team.email") }}</label>
                <input type="email" class="form-control" name="email" value="{{$user->email}}"
                       placeholder="Email address" required="required">
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group user-team-edit">
                            <label for="admin"> Admin</label>
                            <div class="pull-right">
                                {{ Form::checkbox('admin',true, $user->admin) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group user-team-edit">
                            <label for="assistant"> Assistant</label>
                            <div class="pull-right">
                                {{ Form::checkbox('assistant',true, $user->assistant) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-6 col-xs-12">
            <h4>Teams</h4>
            <hr/>
            <div class="row">
                @foreach($teams as $team)
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group user-team-edit">
                            <label for="team">{{ $team->name }}</label>
                            <div class="pull-right">
                                Member
                                {{ Form::checkbox('team[]',$team->id, $team->member) }} |
                                Lead
                                {{ Form::checkbox('team_lead[]',$team->id, $team->lead) }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="col-md-12">
            <hr/>
            <div class="form-group">
                <a href="{{ route('users.index') }}" class="btn btn-default btn-outline">Cancel</a>
                <button class="btn btn-primary"> @busy Update user</button>
            </div>
        </div>

        {{ Form::close() }}
    </div>
@endsection
