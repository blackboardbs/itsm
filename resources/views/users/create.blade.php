@extends('layouts.app')
@section('content')
    <div class="description">
        <h3>Add a new user</h3>
    </div>

    <hr />

    <div class="row">
        <div class="col-md-6">
            {{ Form::open(["url" => route('users.store')]) }}
            <div class="form-group">
                <label for="name">{{ __("team.name") }}</label>
                <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="User name" required="required">
            </div>
            <div class="form-group">
                <label for="email">{{ __("team.email") }}</label>
                <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Email address" required="required">
            </div>
            <div class="form-group">
                <label for="team">Team</label>
                {{Form::select('team',$teams,old('team'),['class'=>'form-control','required'=>'required'])}}
            </div>
            <div class="form-group">
                <label for="lead">Is Team Lead</label>
                {{Form::select('lead',$lead,old('lead'),['class'=>'form-control','required'=>'required'])}}
            </div>
            <div class="form-group">
                <button class="btn btn-primary"> @busy Add new user</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
