@extends('layouts.app')
@section('content')
    <div class="page-title">
        <h3>Resolving Actions</h3>
    </div>

    <div class="row">
        <a class="btn btn-primary" href="{{ route("ac.actions.create") }}">@icon(plus) Actions</a>
    </div>

    @paginator($actions)
    <table class="table table-hover table-striped table-condensed">
        <thead>
        <tr>
            <th> Description</th>
            <th> Steps</th>
            <th> Client Reply </th>
            <th> Updated </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse($actions as $action)
            <tr>
                <td> {{ $action->name}}</td>
                <td> {{ str_limit($action->action_steps, 50, '...')}} </td>
                <td> {{ str_limit(strip_tags($action->client_script), 50, '...') }}</td>
                <td> {{ $action->updated_at->diffForHumans() }}</td>
                <td>
                    @can('update', $action)
                        {{--<a href="{{route('ac.actions.edit', $question)}}">@icon(pencil)</a>--}}
                    @endcan
                        <a class="btn btn-xs btn-default" href="{{route('ac.actions.show', $action)}}">@icon(eye)</a>
                        <a class="btn btn-xs btn-default" href="{{route('ac.actions.edit', $action)}}">@icon(pencil)</a>
                        @include('components.deleteButton', ['url' => 'ac.actions.destroy', 'model' => $action, 'btn_class' => 'btn-xs'])

                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6" class="empty-cell">No Actions Defined</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    @paginator($actions)
@endsection
