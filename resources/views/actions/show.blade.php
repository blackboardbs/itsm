@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ route('ac.actions.index') }}"> Resolving actions </a>
        </div>
    </div>

    <h3 class="field-title">#{{ $action->id }} {{ $action->name }}</h3>

    <div class="row">
        <div class="col-md-8">
            <div class="field-group">
                <label for="action_steps">Action Steps:</label>
                <div class="field-value">
                    {!! nl2br($action->action_steps) !!}
                </div>
            </div>

            <div class="field-group">
                <label for="client_script">Client Script:</label>
                <div class="field-value">
                    {!! $action->client_script !!}
                </div>
            </div>

            <a class="btn btn-default" href="{{ route('ac.actions.index') }}">View Actions</a>
            <a class="btn btn-primary" href="{{route('ac.actions.edit', $action)}}">@icon(pencil) Edit</a>
        </div>
    </div>

@endsection