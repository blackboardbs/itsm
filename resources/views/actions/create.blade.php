@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ route('ac.actions.index') }}"> Resolving Actions </a>
        </div>
    </div>

    <h3>Create resolving actions</h3>

    <div class="row">
        <div class="col-md-6">
            {{ Form::open(["url" => route("ac.actions.store")]) }}

            <div class="form-group">
                <label for="name">Description:</label>
                <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Description"
                       required/>
            </div>

            <div class="form-group">
                <label for="action_steps">Action Steps:</label>
                <textarea class="form-control" name="action_steps" placeholder="Action Steps" rows="3" required>
                    {{ old('action_steps') }}
                </textarea>
            </div>

            <div class="form-group">
                <label for="client_script">Client Reply:</label>
                <textarea class="summernote form-control" name="client_script" placeholder="Client Reply" rows="5"
                          required>
                    {{ old('client_script') }}
                </textarea>
            </div>

            {{--@include('components.mergeFields')--}}

            <button class="btn btn-primary" type="submit"> Save</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('styles')
    <link href="{{ asset('js/summernote/summernote.css') }}" rel="stylesheet">
@endsection
@section('scripts')
    @include('components.js.summernote')
@endsection
