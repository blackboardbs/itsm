@extends('layouts.app')
@section('content')
    <div class="page-title">
        <h3>Resolving ticket</h3>
    </div>

    <div class="row">
        {{ Form::open(["url" => route("ac.resolve.store")]) }}
        <div class="col-md-6">
            <div class="form-group">
                <label for="title">Ticket</label>
                <input type="title" name="title" class="form-control disabled" disabled="disabled"
                       value="{{ $ticket->title }}"/>
            </div>
            <div class="form-group">
                <label for="name">User</label>
                <input type="text" name="name" class="form-control disabled" disabled="disabled"
                       value="{{ $ticket->requester->name }}"/>
            </div>
            <div class="form-group">
                <label for="email">User contact</label>
                <input type="email" name="email" class="form-control disabled" disabled="disabled"
                       value="{{ $ticket->requester->email }}"/>
            </div>
            <div class="form-group">
                <label for="action">Action taken</label>
                {!! Form::select('action', $actions, null , ['id' => 'action_taken','class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                <label for="rootcause">Root cause</label>
                {!! Form::select('rootcause', $rootcause, null , ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                <label for="feedback">Feedback to client</label>
                <textarea name="feedback" class="summernote form-control" id="feedback"></textarea>
            </div>
            <input type="hidden" name="id" value="{{$ticket->id}}"/>
            <div class="form-group">
                <label for="sendfeedback">Send client feedback </label><br/>
                <input type="checkbox" name="sendfeedback" value="mail"> Email
                <input type="checkbox" name="sendfeedback" value="sms" class="disabled" disabled> Sms
                <input type="checkbox" name="sendfeedback" value="call"> Call
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="priority">Priority</label>
                <select type="title" name="title" class="form-control disabled" disabled="disabled">
                    <option value="0">{{$ticket->priorityName() }}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="application">Application</label>
                <select class="form-control disabled" disabled="disabled">
                    <option>{{$ticket->solution->name}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="url">URL</label>
                <input type="text" name="url" class="form-control disabled" value="{{$ticket->url}}" disabled/>
            </div>
            <div class="form-group">
                <label for="attachment">Attachment from customer</label>
                <div class="form-control disabled">@icon(paperclip)<a href="#"></a></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="attachment">Comment attachment</label>
                        <input type="file" name="comment-attachment" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="comments">Additional Comments</label>
                        <textarea name="comments" class="form-control" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="improvement">System improvement</label>
                        <input type="checkbox" name="improvement" id="improvement" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <button class="btn btn-primary" type="submit">Save and send</button>
            <a class="btn btn-default" href="{{url()->previous()}}">Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@section('styles')
    <link href="{{ asset('js/summernote/summernote.css') }}" rel="stylesheet">
@endsection

@section('scripts')
    <script>
        var actions = {!! $actions_json !!}

        $(function () {
            var actionTaken = $('#action_taken');
            setFeedbackText(actionTaken.val());
            actionTaken.change(function () {
                setFeedbackText($(this).val());
            });
        });

        function setFeedbackText(id) {
            var action = actions[id];
            if (action) {
                $('#feedback').html(action);
                if(summernoted)
                    summernoted.summernote("code", action);
            }
        }
    </script>
    @include('components.js.summernote');
@endsection