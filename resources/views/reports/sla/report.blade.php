@extends('layouts.app')
@section('content')
    <h3 class="page-title">SLA Report for {{ Auth::user()->name }}</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Answered Tickets</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-condensed table-bordered">
                <tr>
                    <th>Ticket</th>
                    <th>User</th>
                    <th>Event</th>
                    <th>Time taken</th>
                    <th>SLA</th>
                    <th>Difference</th>
                    <th>Rating</th>
                </tr>
                @forelse($reports as $report)
                    <tr>
                        <td>{{$report->ticket->title}}</td>
                        <td>{{$report->user->name}}</td>
                        <td>{{$report->startEvent()}} <span
                                    style="padding: 0 12px; display: inline-block">-></span> {{$report->endEvent()}}
                        </td>
                        @foreach($report->slaRating() as $report)
                            <td>{!! $report !!}</td>
                        @endforeach
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="7">No Tickets Answered</td>
                    </tr>
                @endforelse
            </table>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Unanswered Tickets</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-condensed table-bordered">
                <tr>
                    <th>Status/Priority</th>
                    <th>Ticket</th>
                    <th>Unanswered since</th>
                </tr>
            @forelse($tickets as $ticket)
                <tr>
                    <td>@include('components.ticket.status',$ticket)</td>
                    <td><a href="{{route('tickets.show',$ticket->id)}}">#{{$ticket->id}} · {{$ticket->title}}</a></td>
                    <td>{{$ticket->created_at->diffForHumans()}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center">Assigned tickets answered</td>
                </tr>
            @endforelse
            </table>
        </div>
    </div>


@endsection