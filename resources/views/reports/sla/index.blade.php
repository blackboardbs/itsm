@extends('layouts.app')
@section('content')
    <h3 class="page-title">Configure SLA's</h3>

    {{ Form::open(["url" => route("sla.store")]) }}
    <table class="table table-hover table-striped table-condensed table-bordered">
        <thead>
        <th>Priority</th>
        <th>Unit</th>
        <th>Measure</th>
        <th>From event</th>
        <th>To event</th>
        </thead>
        <tbody>
        @forelse($configurations as $config)
            <tr>
                <td>{{ __('ticket.' . \App\Ticket::priorityNameFor($config->priority)) }}</td>
                <td class="unit-input">
                    <input type="text" name="unit[{{$config->id}}]" value="{{ $config->unit }}" class="form-control"/>
                </td>
                <td>
                    {{ Form::select('measure['.$config->id.']', \App\SLAConfiguration::measures(), $config->measure , ['class'=>'form-control']) }}
                </td>
                <td>{{ __('ticket.' . \App\Ticket::statusNameFor($config->start_event)) }}</td>
                <td>{{ __('ticket.' . \App\Ticket::statusNameFor($config->end_event)) }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="5" class="text-center">No SLA configurations available, please update master data</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    <button class="btn btn-primary" type="submit">@icon(save) Update Configuration</button>
    {{ Form::close() }}
@endsection