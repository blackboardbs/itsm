@extends('layouts.app')
@section('content')
    <h3 class="page-title">Dashboard</h3>

    <div class="row">
        <div class="col-md-6">
            {{--<div class="chart-loading">@icon(spin fa-spin) Loading...</div>--}}
            <canvas id="ticketsOpened" width="100%"></canvas>
        </div>
        <div class="col-md-6">
            <canvas id="resolutions" width="100%"></canvas>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p class="panel-title">SLA Exceeded</p>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <th>Customer</th>
                        </tr>
                        @foreach($companies as $company)
                            <tr>
                                <td>{{$company->name}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <canvas id="slaTimes" width="100%"></canvas>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/chart.js') }}"></script>
    <script>

        var barOpened = $('#ticketsOpened');
        var myBarChart = new Chart(barOpened, {
            type: 'bar',
            data: {
                labels: [],
                datasets: [{label: 'Tickets opened', data: [], backgroundColor: []}]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        var pieResolutions = $('#resolutions');
        var myPieChart = new Chart(pieResolutions, {
            type: 'doughnut',
            data: {
                datasets: [{data: [], backgroundColor: [],}],
                labels: [
                    'Resolved within SLA',
                    'Resolved outside SLA'
                ]
            }
        });

        var barSLA = $('#slaTimes');
        var my2BarChart = new Chart(barSLA, {
            type: 'bar',
            data: {
                labels: [],
                datasets: [{label: 'Tickets resolved per priority', data: [], backgroundColor: []}]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

        var token = 'the-admin-api-token';

        var getOpened = function (barChart) {
            $.ajax({
                url: '/api/dashboard/opened',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('token', token);
                },
                success: function (response) {

                    barChart.data.labels = response.data.labels;
                    barChart.data.datasets = response.data.datasets;
                    barChart.update();
                },
                error: function (error) {
                    console.log('oh oh', error);
                }
            });
        };

        var getSlaRatio = function (pieChart) {
            $.ajax({
                url: '/api/dashboard/resolved/sla',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('token', token);
                },
                success: function (response) {

                    pieChart.data.labels = response.data.labels;
                    pieChart.data.datasets = response.data.datasets;
                    pieChart.update();
                },
                error: function (error) {
                    console.log('oh oh', error);
                }
            });
        };

        var getPriorityResolved = function (barChart) {
            $.ajax({
                url: '/api/dashboard/resolved/avg',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('token', token);
                },
                success: function (response) {

                    barChart.data.labels = response.data.labels;
                    barChart.data.datasets = response.data.datasets;
                    barChart.update();
                },
                error: function (error) {
                    console.log('oh oh', error);
                }
            });
        };

        getOpened(myBarChart);
        getSlaRatio(myPieChart);
        getPriorityResolved(my2BarChart);

    </script>
@endsection