@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ route('ac.actions.index') }}"> Supported Solutions </a>
        </div>
    </div>

    <h3>#{{ $solution->id }} {{ $solution->name }}</h3>

    <div class="row">
        <div class="col-md-6">
            {{ Form::open(["url" => route('so.solution.update', $solution), 'method' => 'PUT']) }}

            <div class="form-group">
                <label for="name">Solution name:</label>
                <input class="form-control" type="text" name="name" value="{{$solution->name}}" placeholder="Solution name" required/>
            </div>

            <div class="form-group">
                <label for="version">Version:</label>
                <input class="form-control" type="text" name="version" value="{{$solution->version}}" placeholder="Version" required />
            </div>

            <div class="form-group">
                <label for="product_owner">Product owner:</label>
                <input class="form-control" type="text" name="product_owner" value="{{$solution->product_owner}}" placeholder="Product owner" required />
            </div>

            <a href="{{route('so.solution.index')}}" class="btn btn-default"> Cancel</a>
            <button class="btn btn-primary" type="submit"> Update</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
