@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ route('so.solution.index') }}"> Supported Solutions</a>
        </div>
    </div>

    <h3>Create a supported solution</h3>

    <div class="row">
        <div class="col-md-6">
            {{ Form::open(["url" => route("so.solution.store")]) }}

            <div class="form-group">
                <label for="name">Solution name:</label>
                <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Solution name"
                       required/>
            </div>

            <div class="form-group">
                <label for="version">Version:</label>
                <input class="form-control" type="text" name="version" value="{{ old('version') }}" placeholder="Version" required />
            </div>

            <div class="form-group">
                <label for="product_owner">Product owner:</label>
                <input class="form-control" type="text" name="product_owner" value="{{ old('product_owner') }}" placeholder="Product owner" required />
            </div>

            {{--@include('components.mergeFields')--}}

            <button class="btn btn-primary" type="submit"> Save</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
