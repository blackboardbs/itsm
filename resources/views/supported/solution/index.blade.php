@extends('layouts.app')
@section('content')
    <div class="page-title">
        <h3>Supported Solutions</h3>
    </div>

    <div class="row">
        <a class="btn btn-primary" href="{{ route("so.solution.create") }}">@icon(plus) Solution</a>
    </div>

    @paginator($solutions)
    <table class="table table-hover table-striped table-condensed">
        <thead>
        <tr>
            <th> Name</th>
            <th> Version</th>
            <th> Product Owner</th>
            <th> Updated </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse($solutions as $solution)
            <tr>
                <td> {{ $solution->name}}</td>
                <td> {{ $solution->version}} </td>
                <td> {{ $solution->product_owner}}</td>
                <td> {{ $solution->updated_at->diffForHumans() }}</td>
                <td>
                    @can('update', $solution)
                        {{--<a href="{{route('co.company.edit', $question)}}">@icon(pencil)</a>--}}
                    @endcan
                        <a class="btn btn-xs btn-default" href="{{route('so.solution.edit', $solution)}}">@icon(pencil)</a>
                        @include('components.deleteButton', ['url' => 'so.solution.destroy', 'model' => $solution, 'btn_class' => 'btn-xs'])
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6" class="empty-cell">No Solutions Defined</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    @paginator($solutions)
@endsection
