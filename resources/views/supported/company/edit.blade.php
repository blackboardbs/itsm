@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ route('ac.actions.index') }}"> Supported Companies </a>
        </div>
    </div>

    <h3>#{{ $company->id }} {{ $company->name }}</h3>

    <div class="row">
        <div class="col-md-6">
            {{ Form::open(["url" => route('co.company.update', $company), 'method' => 'PUT']) }}

            <div class="form-group">
                <label for="name">Company name:</label>
                <input class="form-control" type="text" name="name" value="{{$company->name}}" placeholder="Company name" required/>
            </div>

            <div class="form-group">
                <label for="address">Address:</label>
                <input class="form-control" type="text" name="address" value="{{$company->address}}" placeholder="Address" required />
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="email">Email:</label>
                    <input class="form-control" type="email" name="email" value="{{$company->email}}" placeholder="Email" required />
                </div>

                <div class="form-group col-md-6">
                    <label for="support_email">Support Email:</label>
                    <input class="form-control" type="email" name="support_email" value="{{$company->support_email}}" placeholder="Support Email" required />
                </div>
            </div>

            <div class="form-group">
                <label for="web">Web:</label>
                <input class="form-control" type="url" name="web" value="{{$company->web}}" placeholder="Web" required />
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="phone">Phone:</label>
                    <input class="form-control" type="tel" name="phone" value="{{$company->phone}}" placeholder="Phone" required />
                </div>

                <div class="form-group col-md-6">
                    <label for="contact_number">Contact Number:</label>
                    <input class="form-control" type="text" name="contact_number" value="{{$company->contact_number}}" placeholder="Contact Number" required/>
                </div>
            </div>

            <a href="{{route('co.company.index')}}" class="btn btn-default"> Cancel</a>
            <button class="btn btn-primary" type="submit"> Update</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('styles')
    <link href="{{ asset('js/summernote/summernote.css') }}" rel="stylesheet">
@endsection
@section('scripts')
    @include('components.js.summernote')
@endsection
