@extends('layouts.app')
@section('content')
    <div class="page-title">
        <h3>Supported Companies</h3>
    </div>

    <div class="row">
        <a class="btn btn-primary" href="{{ route("co.company.create") }}">@icon(plus) Company</a>
    </div>

    @paginator($companies)
    <table class="table table-hover table-striped table-condensed">
        <thead>
        <tr>
            <th> Name</th>
            <th> Contact Number</th>
            <th> Support Email</th>
            <th> Ticket Create Url</th>
            <th> Updated</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse($companies as $company)
            <tr>
                <td> {{ $company->name}}</td>
                <td> {{ $company->contact_number}} </td>
                <td> {{ $company->support_email}}</td>
                <td> {{ route('requester.tickets.create', ['company' => $company->id]) }}</td>
                <td> {{ $company->updated_at->diffForHumans() }}</td>
                <td>
                    @can('update', $company)
                        {{--<a href="{{route('co.company.edit', $question)}}">@icon(pencil)</a>--}}
                    @endcan
                    <a class="btn btn-xs btn-default" href="{{ route('tickets.index', ['company' => $company->id]) }}">
                        @icon(inbox) Tickets
                    </a>
                    <a class="btn btn-xs btn-default" href="{{route('co.company.edit', $company)}}">@icon(pencil)</a>
                    @include('components.deleteButton', ['url' => 'co.company.destroy', 'model' => $company, 'btn_class' => 'btn-xs'])
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6" class="empty-cell">No Companies Defined</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    @paginator($companies)
@endsection
