@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ route('ac.actions.index') }}"> Supported Companies </a>
        </div>
    </div>

    <h3>#{{ $customer->id }} {{ $customer->name }}</h3>

    <div class="row">
        <div class="col-md-6">
            {{ Form::open(["url" => route('cu.customer.update', $customer), 'method' => 'PUT']) }}

            <div class="form-group">
                <label for="name">Customer name:</label>
                <input class="form-control" type="text" name="name" value="{{$customer->name}}" placeholder="Customer name" required/>
            </div>

            <div class="form-group">
                <label for="address">Address:</label>
                <input class="form-control" type="text" name="address" value="{{$customer->address}}" placeholder="Address" required />
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="email">Email:</label>
                    <input class="form-control" type="email" name="email" value="{{$customer->email}}" placeholder="Email" required />
                </div>

                <div class="form-group col-md-6">
                    <label for="phone">Phone:</label>
                    <input class="form-control" type="tel" name="phone" value="{{$customer->phone}}" placeholder="Phone" required />
                </div>
            </div>

            <div class="form-group">
                <label for="company_id">Company</label>
                {!! Form::select('company_id', $companies, $customer->company_id , ['class'=>'form-control']) !!}
            </div>

            <a href="{{route('cu.customer.index')}}" class="btn btn-default"> Cancel</a>
            <button class="btn btn-primary" type="submit"> Update</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
