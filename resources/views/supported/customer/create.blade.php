@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ route('cu.customer.index') }}"> Supported Customers</a>
        </div>
    </div>

    <h3>Create a supported customer</h3>

    <div class="row">
        <div class="col-md-6">
            {{ Form::open(["url" => route("cu.customer.store")]) }}

            <div class="form-group">
                <label for="name">Customer name:</label>
                <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Customer name"
                       required/>
            </div>

            <div class="form-group">
                <label for="address">Address:</label>
                <input class="form-control" type="text" name="address" value="{{ old('address') }}" placeholder="Address" required />
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="email">Email:</label>
                    <input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="Email" required />
                </div>

                <div class="form-group col-md-6">
                    <label for="phone">Phone:</label>
                    <input class="form-control" type="tel" name="phone" value="{{ old('phone') }}" placeholder="Phone" required />
                </div>
            </div>

            <div class="form-group">
                <label for="company_id">Company</label>
                {!! Form::select('company_id', $companies, null , ['class'=>'form-control']) !!}
            </div>

            {{--@include('components.mergeFields')--}}

            <button class="btn btn-primary" type="submit"> Save</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
