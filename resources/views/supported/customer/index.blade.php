@extends('layouts.app')
@section('content')
    <div class="page-title">
        <h3>Supported Customers</h3>
    </div>

    <div class="row">
        <a class="btn btn-primary" href="{{ route("cu.customer.create") }}">@icon(plus) Customer</a>
    </div>

    @paginator($customers)
    <table class="table table-hover table-striped table-condensed">
        <thead>
        <tr>
            <th> Name</th>
            <th> Contact details</th>
            <th> Company</th>
            <th> Updated </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse($customers as $customer)
            <tr>
                <td>
                    <span style="font-weight: 400">{{ $customer->name}}</span>
                    <span class="text-muted" style="display: block">{{ $customer->address}}</span>
                </td>
                <td>
                    <span>{{ $customer->email}}</span>
                    <span class="text-muted" style="display: block">{{ $customer->phone}}</span>

                </td>
                <td>
                    @if($customer->company != null)
                        {{ $customer->company->name}}
                    @endif
                </td>
                <td> {{ $customer->updated_at->diffForHumans() }}</td>
                <td>
                    @can('update', $customer)
                        {{--<a href="{{route('co.company.edit', $question)}}">@icon(pencil)</a>--}}
                    @endcan
                        <a class="btn btn-xs btn-default" href="{{ route('tickets.index', ['requester_id' => $customer->id]) }}">@icon(inbox) Tickets</a>
                        <a class="btn btn-xs btn-default" href="{{route('cu.customer.edit', $customer)}}">@icon(pencil)</a>
                        @include('components.deleteButton', ['url' => 'cu.customer.destroy', 'model' => $customer, 'btn_class' => 'btn-xs'])
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6" class="empty-cell">No Customers Defined</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    @paginator($customers)
@endsection
