@extends('layouts.requester')
@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <a class="list-group-item active">@icon(plus) Open Ticket</a>
            </div>
        </div>
        <div class="col-md-9">
            <h1 class="page-title">Create Ticket</h1>
            <div class="panel panel-info">
                <div class="panel-body">
                    {{ Form::open(["url" => route("requester.tickets.store")]) }}
                    <input type="hidden" value="{{ $company }}" name="company" />
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" value="{{  old('name') }}" required/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" value="{{  old('email') }}" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title">Subject</label>
                        <input type="text" class="form-control" name="title" value="{{ old('title') }}" required />
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="solution_id">Application:</label>
                            {{ Form::select('solution_id', $solutions, null , ['class'=>'form-control']) }}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tags">URL:</label>
                            <input type="text" name="url" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="body">Message</label>
                        <textarea class="form-control" name="body" rows="5" required>{{ old('body') }}</textarea>
                    </div>

                    {{--<div class="row">--}}
                        {{--<div class="form-group col-sm-6">--}}
                            {{--<label for="department">Department</label>--}}
                            {{--{!! Form::select('department', $teams, old('department') , ['class'=>'form-control']) !!}--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-sm-6">--}}
                            {{--<label for="priority">Priority</label>--}}
                            {{--{!! Form::select('priority', $priority, old('priority') , ['class'=>'form-control']) !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="route">Category</label>
                            {!! Form::select('route', $configs, old('route') , ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="attachment">@icon(paperclip) Attachment</label>
                            <input type="file" name="attachment" id="inputAttachment" class="form-control">
                        </div>
                    </div>

                    <button class="btn btn-primary" type="submit"> @busy Submit</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
