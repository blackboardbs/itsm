@extends('layouts.requester')
@section('content')

    <div class="row">
        @if($shouldAutoClose == true)
            <div class="col-md-8 col-md-offset-2">
                <h4>Locked as there has been no response in over 72 hours.</h4>
                <p>To unlock please just reply to the email ticket sent to you.</p>
                <a class="btn btn-default"
                   href="mailto:{{$supportEmail}}?subject=Unlock ticket: #{{$ticket->id}}: {{$ticket->title}}&body=##- Please type your reply above this line -## ticket-id:{{$ticket->id}} ">
                    @icon(unlock) Unlock Ticket
                </a>
            </div>
        @else
            <div class="col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Ticket Information</h3>
                    </div>
                    <div class="list-group">
                        <div class="list-group-item">
                            <div class="truncate">#{{ $ticket->id }} - {{ $ticket->title }}</div>
                            {!! ticketStatus($ticket->status, $ticket->statusName()) !!}
                        </div>
                        <div class="list-group-item">
                            <label>Department</label>
                            <div class="value"> {{$ticket->team->name}}</div>
                        </div>
                        <div class="list-group-item">
                            <label>Submitted</label>
                            <div class="value"> {{$ticket->created_at }}</div>
                        </div>
                        <div class="list-group-item">
                            <label>Last updated</label>
                            <div class="value"> {{$ticket->updated_at->diffForHumans() }}</div>
                        </div>
                        <div class="list-group-item">
                            <label>Priority</label>
                            <div class="value"> {{$ticket->priorityName() }}</div>
                        </div>
                    </div>
                    <div class="panel-footer clearfix">
                        <button class="btn btn-primary halfwidth left" onclick="jQuery('#replyTicket').click()">Reply
                        </button>
                        <button class="btn btn-warning halfwidth right">Close</button>
                    </div>
                </div>
                <div class="list-group">
                    <a class="list-group-item" href="{{route('requester.tickets.create')}}">@icon(plus) Open Ticket</a>
                </div>
            </div>
            <div class="col-md-9">
                <h1 class="page-title">View Ticket</h1>

                <div class="panel panel-info panel-collapsable hidden-print panel-collapsed">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a data-toggle="collapse" href="#collapse1" id="replyTicket">
                                @icon(pencil)
                                Reply
                                <span class="collapse-icon pull-right">
                                @icon(plus)
                            </span>
                            </a>
                        </h3>
                    </div>
                    <div class="panel-body panel-collapse collapse" id="collapse1" aria-expanded="false">
                        @if($ticket->status != App\Ticket::STATUS_CLOSED)
                            <div class="comment new-comment">
                                {{ Form::open(["url" => route("requester.comments.store",$ticket->public_token)]) }}
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control disabled" name="name"
                                               value="{{  $ticket->requester->name }}" disabled/>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control disabled" name="email"
                                               value="{{  $ticket->requester->email }}" disabled/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="body">Message</label>
                                    <textarea class="form-control" name="body" rows="5" required></textarea>
                                </div>
                                <div class="form-group">
                                    @if($ticket->status == App\Ticket::STATUS_SOLVED)
                                        {{ __('ticket.reopen') }} ? {{ Form::checkbox('reopen') }}
                                    @else
                                        {{ __('ticket.isSolvedQuestion') }} {{ Form::checkbox('solved') }}
                                    @endif
                                </div>
                                <button class="btn btn-primary" type="submit"> @busy Submit</button>
                                <a class="btn btn-default" href="#" onclick="jQuery('#replyTicket').click()"> Cancel</a>
                                {{ Form::close() }}
                            </div>
                        @endif
                    </div>
                </div>

                @include('components.ticketComments', ["comments" => $ticket->comments])
            </div>
        @endif
    </div>
@endsection

@section('script')

@endsection
