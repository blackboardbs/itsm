<li class="{{ str_contains(request()->fullUrlWithQuery([]), $url) ? "active" : "" }}">
    <a href="{{ $url }}">
        @if( isset($icon))
            @icon({{$icon}})
        @endif
        {{ $title }}

        @if( isset($count) )
            @include('components.sidebarLabel', compact("count"))
        @endif
    </a>
</li>