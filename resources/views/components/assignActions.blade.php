<div class="panel panel-default panel-collapsable hidden-print panel-collapsed">
    <div class="panel-heading">
        <h3 class="panel-title">
            <a data-toggle="collapse" href="#collapse1" id="replyTicket">
                @icon(pencil)
                Re-assign ticket
                <span class="collapse-icon pull-right">
                    @icon(plus)
                </span>
            </a>
        </h3>
    </div>
    <div class="panel-body panel-collapse collapse" id="collapse1" aria-expanded="false">
        {{ Form::open(["url" => route("{$endpoint}.assign", $object)]) }}
        <div class="form-group">
            <label for="tags"> {{ trans_choice('ticket.tag',2) }}:</label>
            <input class="form-control" id="tags" name="tags" value="{{$object->tagsString()}}">
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    @can("assignToTeam", $object)
                        <label for="user_id">{{ __('ticket.assigned') }}:</label>
                        {{ Form::select('user_id', App\Team::membersByTeam(), $object->user_id, ['class' => 'form-control']) }}
                    @else
                        @if ($object->team)
                            <label for="user_id">{{ __('ticket.assigned') }}:</label>
                            {{ Form::select('user_id', createSelectArray( $object->team->members, true), $object->user_id, ['class' => 'form-control']) }}
                        @endif
                    @endcan
                </div>
            </div>
            <div class="col-md-6">
                @can("assignToTeam", $object)
                    @include('components.assignTeamField', ["team" => $object->team])
                @endcan
            </div>
        </div>
        <div class="form-group">
            <button class="btn btn-default"> {{ __('ticket.assign') }}</button>
        </div>
        {{ Form::close() }}
    </div>
</div>
