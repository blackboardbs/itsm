@php if (! isset($team)) $team = new App\Team; @endphp
<div class="form-group">
    <label for="team_id">{{ trans_choice('team.team',1) }}:</label>
    @can("assignToTeam", new App\Ticket)
        {{ Form::select('team_id', createSelectArray( App\Team::all(), true), $team->id, ['class' => 'form-control']) }}
    @else
        {{ Form::select('team_id', createSelectArray( auth()->user()->teams, false), $team->id,  ['class' => 'form-control']) }}
    @endcan
</div>