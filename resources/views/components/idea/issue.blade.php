@if( config('issues.driver') && count( config('issues.repositories')) > 0)
    @if(auth()->user()->assistant || auth()->user()->admin)
        @if($idea->issue_id)
            <div class="col-md-3 pull-right">
                <a class="btn btn-default" href="{{$idea->issueUrl()}}" target="_blank"> @icon(bug) {{ __('ticket.seeIssue') }}</a>
            </div>
        @elseif($idea->repository)
            <div class="col-md-3 pull-right">
                {{ Form::open(["url" => route('ideas.issue.store', $idea)]) }}
                <button class="btn btn-default dropdown"> @icon(bug) {{ __('ticket.createIssue') }}</button>
                {{ Form::close() }}
            </div>
        @endif
    @endif
@endif
