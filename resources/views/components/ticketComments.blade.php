    @foreach($comments as $comment)
        @if($comment instanceof App\TicketEvent)
            @include('components.ticketEvent', ["event" => $comment])
        @else
            <div class="panel @if($comment->_role() === 'Staff') panel-success @else panel-info @endif  comment @if($comment->private) note @endif">
                <div class="panel-heading">
                    <div class="panel-title">
                        @if($comment->_role() === 'Staff') @icon(users) @else @icon(user) @endif
                        <span class="label label-default">{{$comment->_role()}}</span>
                        {{ $comment->author()->name }}
                    </div>
                    <div class="right">{{ $comment->created_at->diffForHumans() }}</div>
                </div>
                <div class="panel-body">
                    @if($comment->private) @icon(sticky-note-o) @endif
                    {!! nl2br( strip_tags($comment->body)) !!}
                </div>
                @include('components.attachments', ["attachments" => $comment->attachments])
            </div>
        @endif
    @endforeach

    <div class="panel panel-info comment">
        <div class="panel-heading">
            <div class="panel-title">@icon(user) {{ $ticket->requester->name }} </div>
            <div class="right">{{ $ticket->created_at->diffForHumans() }}</div>
        </div>
        <div class="panel-body">{!! nl2br( strip_tags($ticket->body)) !!} </div>
        @include('components.attachments', ["attachments" => $ticket->attachments])
    </div>