@if($ticket->isEscalated() )
    @if($ticket->status < App\Ticket::STATUS_CLOSED)
        <div class="alert alert-danger">
            {{ Form::open(["url" => route('tickets.escalate.destroy', $ticket), "method" => "delete" ]) }}
            @icon(flag) {!! __('ticket.escalatedDesc') !!}
            <button class="btn btn-default btn-sm">@icon(flag) {{ __('ticket.de-escalate') }}</button>
            {{ Form::close() }}
        </div>
    @endif
@else
    <div class="pull-right col-md-2">
    {{ Form::open(["url" => route('tickets.escalate.store', $ticket) ]) }}
        <button class="btn btn-default btn-sm">@icon(flag) {{ __('ticket.escalate') }}</button>
    {{ Form::close() }}
    </div>
@endif
