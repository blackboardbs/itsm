    @if($ticket->status == App\Ticket::STATUS_SOLVED)
        <div class="col-md-3 pull-right">
            {{ Form::open(["url" => route('tickets.reopen', $ticket)]) }}
            <button class="btn btn-default"> {{ __('ticket.reopen') }}</button>
            {{ Form::close() }}
        </div>
    @else
        <div class="col-md-4">
            @include('components.ticket.idea')
            @include('components.ticket.issue')
            @include('components.ticket.escalate')
        </div>
    @endif