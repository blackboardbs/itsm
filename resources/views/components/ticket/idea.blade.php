@if( config('handesk.roadmap') )
    @if($ticket->getIdeaId())
        <div class="col-md-3 pull-right">
            <a class="btn btn-default btn-sm" href="{{route('ideas.show',["id" => $ticket->getIdeaId()])}}" target="_blank"> @icon(lightbulb-o) {{ __('ticket.seeIdea') }}</a>
        </div>
    @else
        <div class="col-md-3 pull-right">
        {{ Form::open(["url" => route('tickets.idea.store', $ticket)]) }}
            <button class="btn btn-default"> @icon(lightbulb-o) {{ __('ticket.createIdea') }}</button>
        {{ Form::close() }}
        </div>
    @endif
@endif

