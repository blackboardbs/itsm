@if( config('issues.driver') && count( config('issues.repositories')) > 0)
    @if(auth()->user()->assistant || auth()->user()->admin)
        @if($ticket->getIssueId())
            <div class="btn-group" role="group">
                <a class="btn btn-default btn-sm" href="{{$ticket->issueUrl()}}" target="_blank"> @icon(bug) {{ __('ticket.seeIssue') }}</a>
            </div>
        @else
            <div class="btn-group" role="group">
                <div class="dropdown">
                    <button class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @icon(bug) {{ __('ticket.createIssue') }}
                        <span class="caret"></span>
                    </button>
                </div>
                <ul class="dropdown-menu">
                    @foreach(config('issues.repositories') as $name => $repo)
                        <li><a onClick="createIssueToRepo('{{$repo}}')"> {{ $name }}</a></li>
                    @endforeach
                </ul>
            {{ Form::open(["url" => route('tickets.issue.store', $ticket), "id" => "issue-form"]) }}
                {{ Form::hidden('repository',"", ["id" => "issue-repository"]) }}
            {{ Form::close() }}
            </div>
        @endif
    @endif

    <script>
        function createIssueToRepo(repo){
            $('#issue-repository').val(repo);
            $('#issue-form').submit();
        }
    </script>
@endif
