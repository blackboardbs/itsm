<span class="label ticket-status-{{ $ticket->statusName() }}">
    {{ link_to_route('tickets.index', __('ticket.' . $ticket->statusName()), ['status' => $ticket->status], '') }}
</span>&nbsp;

<span class="label ticket-priority-{{ $ticket->priorityName() }}">
    {{ link_to_route('tickets.index', __('ticket.' . $ticket->priorityName()), ["priority" => $ticket->priority], '') }}
</span>&nbsp
@if ($ticket->isEscalated()) @icon(flag) @endif
@if ($ticket->getIssueId()) @icon(bug) @endif