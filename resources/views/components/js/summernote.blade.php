<script src="{{ asset('js/summernote/summernote.min.js') }}"></script>
<script type="text/javascript">
    var summernoted = null;
    $(document).ready(function () {
        summernoted = $('.summernote').summernote({
            height: 150,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link']],
                ['height', ['height']],
                ['misc', ['codeview']]
            ]
        });
    });
</script>