{{ Form::open(["url" => route($url, $model), 'method' => 'DELETE', 'class'=>'delete_form']) }}
    <button type="submit" class="btn btn-danger {{$btn_class}}">
        @icon(trash-o)
        @if( isset($text))
            @icon({{$text}})
        @endif
    </button>
{{ Form::close() }}