<div class="form-group">
    <label for="attachment">@icon(paperclip) Attachment</label>
    {{ Form::file('attachment', ["id" => "attachment", 'class'=>'form-control']) }}
</div>
