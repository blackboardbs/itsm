
@if($user instanceof App\User)
    {!! profilePhoto(false,$user->photo) !!}
    @if($user->assistant)
        <div class="inline ml-3 gold">@icon(star)</div>
    @endif
@else
    <img src="/user.png" class="" />
@endif