<div class="panel panel-warning">
    <div class="panel-body">
        {{--@icon(dot-circle-o)--}}
        {{ $event->author()->name }}
        •
        {!! nl2br( strip_tags($event->body)) !!}
        •
        {{ $event->created_at->diffForHumans() }}
    </div>
</div>