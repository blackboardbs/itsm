@extends('layouts.app')
@section('content')

    <h3> {{ trans_choice('setting.setting',2) }}</h3>

    <div class="clear-both"></div>

    <div class="row">
        <div class="col-md-8">
            <table class="table table-condensed">
                <tr>
                    <td>Api token:</td>
                    <td><span id="api-token">{{ config('handesk.api_token') }}</span>
                        <button class="btn btn-default" onclick="copyToClipboard('#api-token')"> @icon(clipboard) Copy to clipboard
                        </button>
                    </td>
                </tr>
                {{ Form::open(["url" => route('settings.update',$settings), "method" => "PUT"]) }}
                <tr>
                    <td>{{ __("team.slack_webhook_url") }}:</td>
                    <td>
                        <input class="form-control" name="slack_webhook_url" value="{{$settings->slack_webhook_url}}">
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><span class="lighter-gray fs2">{{ __('team.slack_webhook_urlDesc') }}</span></td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-primary">@busy {{ __('ticket.update') }}</button>
                    </td>
                </tr>
                {{ Form::close() }}
            </table>
        </div>
    </div>
@endsection
