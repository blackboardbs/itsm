@extends('layouts.app')
@section('content')
    <div class="description comment">
        <div class="breadcrumb">
            <a href="{{ route('qs.questions.index') }}"> Help configuration </a>
        </div>
    </div>

    <h3>#{{ $question->id }} {{ $question->question }}</h3>

    <div class="row">
        <div class="col-md-6">
            {{ Form::open(["url" => route('qs.questions.update', $question), 'method' => 'PUT']) }}

            <div class="form-group">
                <label for="question">Question:</label>
                <input class="form-control" type="text" name="question" value="{{$question->question}}" placeholder="Question" required />
            </div>

            <div class="form-group">
                <label for="route">Route to:</label>
                {{Form::select('route',$routes,$question->route,['class'=>'form-control'])}}
            </div>

            <div class="form-group">
                <label for="team">Team:</label>
                {{Form::select('team',$teams,$question->team,['class'=>'form-control'])}}
            </div>

            <div class="form-group">
                <label for="priority">Priority:</label>
                {{Form::select('priority',$priority,$question->priority,['class'=>'form-control'])}}
            </div>

            <button class="btn btn-primary" type="submit"> Save</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection


