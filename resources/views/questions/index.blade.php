@extends('layouts.app')
@section('content')
    <div class="page-title">
        <h3>Help Configuration</h3>
    </div>

    <div class="row">
        <a class="btn btn-primary" href="{{ route("qs.questions.create") }}">@icon(plus) Questions Routing</a>
    </div>

    @paginator($questions)
    <table class="table table-hover table-striped table-condensed">
        <thead>
        <tr>
            <th> Question</th>
            <th> Route</th>
            <th> Priority </th>
            <th> Team </th>
            <th> Created</th>
            <th> Updated </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($questions as $question)
            <tr>
                <td> {{ $question->question}}</td>
                <td> {{ $question->route}} </td>
                <td> {{ $question->priority }}</td>
                <td> {{ $question->hteam->name }}</td>
                <td> {{ $question->created_at->diffForHumans() }}</td>
                <td> {{ $question->updated_at->diffForHumans() }}</td>
                <td>
                    @can('update', $question)
                        {{--<a href="{{route('qs.questions.edit', $question)}}">@icon(pencil)</a>--}}
                    @endcan
                        <a href="{{route('qs.questions.edit', $question)}}">@icon(pencil)</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @paginator($questions)
@endsection
