@extends('emails.layout')

@section('body')
    <div style="border-bottom:1px solid #efefef; padding-bottom:10px;">
        <span style="font-size:16px;font-weight: bold">{{ $title }}</span>
    </div>

    <div style="padding: 6px">
        A new user has been created for you on {{ env('APP_NAME') }}<br/><br/>
        <strong>Username:</strong> {{ $user->name }}<br>
        <strong>Email:</strong> {{ $user->email }}
    </div>

    <div style="margin-top:20px">
        Create your password by using <a href="{{$url}}">the password reset link</a>
    </div>

@endsection