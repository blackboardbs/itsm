@extends('layouts.requester')
@section('content')
    <div class="container" style="text-align: center;margin-top:120px">
        <img src="{{url("images/not_found.png")}}">
        <p>Sorry, but the resource you are looking for doesn't exist</p>
    </div>
@endsection