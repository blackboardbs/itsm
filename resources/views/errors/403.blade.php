@extends('layouts.requester')
@section('content')
    <div class="container" style="text-align: center;margin-top:100px">
        <h1>403</h1>
        <img src="{{url("images/locked.png")}}">
        <p>Sorry, but you don't have access to this resource</p>
    </div>
@endsection